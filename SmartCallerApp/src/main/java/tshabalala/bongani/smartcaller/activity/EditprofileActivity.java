package tshabalala.bongani.smartcaller.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import tshabalala.bongani.smartcaller.R;
import tshabalala.bongani.smartcaller.helper.Constants;
import tshabalala.bongani.smartcaller.helper.Subscriber;
import tshabalala.bongani.smartcaller.helper.Utils;

/**
 * Created by Bongani on 2017/08/23.
 */

public class EditprofileActivity extends AppCompatActivity implements TextWatcher {


    EditText etProvince, etCity, etID, etGender,etAge;
    String strProvince,strCity, id, gender;

    String name;
    private EditText mName;
    private EditText mSurname;
    private EditText mPhone;

    String surname ;
    String phone;
    String yob = "";
    int y = 0;


    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    private static final String TAG_USER = "sub";
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;

    Subscriber subscriber;
    String phoneNumber;
    int yourAge = 0;
    Utils utils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        utils = new Utils(this);
        phoneNumber = getIntent().getStringExtra("phone");

        mName = (EditText) findViewById(R.id.name);
        mSurname = (EditText) findViewById(R.id.surname);
        mPhone = (EditText) findViewById(R.id.phone);
        etProvince = (EditText) findViewById(R.id.province);

        etCity = (EditText) findViewById(R.id.city);
        etID = (EditText) findViewById(R.id.idNumber);
        etGender = (EditText) findViewById(R.id.gender);
        etAge = (EditText) findViewById(R.id.age);

        mName.addTextChangedListener(this);
        mSurname.addTextChangedListener(this);


        getSubscriberTast(phoneNumber);

        Button btnRegister = (Button) findViewById(R.id.button_update);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptRegistration();
            }
        });

        Button btnBack = (Button) findViewById(R.id.button_back);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



    }

    private void attemptRegistration() {

        name = mName.getText().toString();
        surname = mSurname.getText().toString();
        phone = mPhone.getText().toString();
        strProvince = etProvince.getText().toString();

        strCity = etCity.getText().toString();

        id = etID.getText().toString();
        gender = etGender.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(strCity)) {
            etCity.setError("Error - city can't be empty");
            focusView = etCity;
            cancel = true;

        }

        if (TextUtils.isEmpty(strProvince)) {
            etProvince.setError("Error - province can't be empty");
            focusView = etProvince;
            cancel = true;

        }

        if (TextUtils.isEmpty(id)) {
            etID.setError("Error - id number can't be empty");
            focusView = etID;
            cancel = true;

        }

        if (!TextUtils.isEmpty(id)) {
            if (!utils.isIDValid(id)) {
                etID.setError("Enter correct ID number");
                focusView = etID;
                cancel = true;

            }
        }




        if (TextUtils.isEmpty(name)) {
            mName.setError("Error - name can't be empty");
            focusView = mName;
            cancel = true;

        }

        if (TextUtils.isEmpty(surname)) {
            mSurname.setError("Error - surname can't be empty");
            focusView = mSurname;
            cancel = true;

        }

        if (TextUtils.isEmpty(phone)) {
            mPhone.setError("Error - cellphone can't be empty");
            focusView = mPhone;
            cancel = true;

        }

        if (!TextUtils.isEmpty(phone)) {
            if (!utils.validatePhone(phone)) {
                mPhone.setError("Error - Enter correct cellphone");
                focusView = mPhone;
                cancel = true;

            }
        }

        if(id != null) {
            try {
                final String year = id.substring(0, 2);
                String month = id.substring(2, 4);
                String day = id.substring(4, 6);

                int yearDate = Integer.parseInt(year);
                int monthDate = Integer.parseInt(month);
                int dayDate = Integer.parseInt(day);

                GregorianCalendar cal = new GregorianCalendar();

                int  m, d, a;

                y = cal.get(Calendar.YEAR);
                m = cal.get(Calendar.MONTH) +1;
                d = cal.get(Calendar.DAY_OF_MONTH);
                cal.set(yearDate, monthDate, dayDate);

                a = y - cal.get(Calendar.YEAR);
                Log.w("TAG", "age "+ a);
                String strAge = String.valueOf(a).substring(2,String.valueOf(a).length());

              int  age = Integer.parseInt(strAge);
                Log.w("TAG", "age "+ age);

                if ( age <= 17)
                {

                    focusView = etID;
                    cancel = true;
                    etID.setError("Error - Too young to register");

                }

                String y1,y2,y3c,y4c,y3n4c;
                y1 = "0";
                y2 = "0";


                y3c = String.valueOf(y).valueOf(String.valueOf(y).charAt(0));
                y4c = String.valueOf(y).valueOf(String.valueOf(y).charAt(1));

                y3n4c = y3c +y4c;


                int calcy3c = Integer.parseInt(y3n4c);

                int caly3n = Integer.parseInt(year);

                if(caly3n > calcy3c)
                {
                    y1 = "1";

                    y2 = "9";
                }

                if(caly3n >=0 && caly3n <= calcy3c)
                {
                    y1 = "2";

                    y2 = "0";
                }

                yob = y1 +y2 + year;

                Log.w("HEllo","LEvels "+age+" grtg "+ yearDate + " ddf "+y+" ff "+year+ " fddf "+ yob);




                if (Integer.parseInt(yob) > y)
                {

                    focusView = etID;
                    cancel = true;
                    etID.setError("Error - Future ID number not allowed");

                }




                System.out.println("year is "+ m+" " + monthDate);
                if (monthDate == 0)
                {
                    etID.setError("Error - Enter correct ID number");
                    focusView = etID;
                    cancel = true;
                }
                if(monthDate == 2)
                {
                    if(dayDate == 30 || dayDate ==31)
                    {
                        etID.setError("Error - Enter correct ID number");
                        focusView = etID;
                        cancel = true;
                    }
                }
                System.out.println("year is "+ d+" " + dayDate);
                if (dayDate == 0)
                {
                    etID.setError("Error - Enter correct ID number");
                    focusView = etID;
                    cancel = true;
                }

            }catch (Exception x)
            {
                x.getMessage();
            }
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            if (!utils.isOnline(EditprofileActivity.this)) {
                utils.toast(getString(R.string.no_internet_connection));
           } else {

                yourAge = y - Integer.parseInt(yob);
                EditProfileTask(yourAge);

            }
        }
    }

    /**
     * Represents an asynchronous EditSubscriber task used to authenticate
     * the user.
     * @param yourAge
     */
    public void EditProfileTask(final int yourAge) {

        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage(getString(R.string.update_subscriber));
        pd.show();

        final StringRequest request = new StringRequest(Request.Method.POST, Constants.EDIT_SUBSCRIBER_URL, new Response.Listener<String>(){
            @Override
            public void onResponse(String success) {
                if (success != null){
                    Log.i("Website Content", success);
                    try {
                        JSONObject jsonPart = new JSONObject(success);
                        int num = jsonPart.getInt(TAG_SUCCESS);
                        String message = jsonPart.getString(TAG_MESSAGE);

                        if (num == 1) {

                            Intent intent = new Intent(EditprofileActivity.this,MainActivity.class);
                            startActivity(intent);

                        } else {

                            Toast.makeText(EditprofileActivity.this, message, Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                pd.dismiss();
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                System.out.println("Erroe" + volleyError);
                utils.toast(volleyError.getMessage());
                pd.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> jsonObject = new HashMap<>();

                jsonObject.put("name", name);
                jsonObject.put("surname", surname);
                jsonObject.put("phone", phone);
                jsonObject.put("province", strProvince);
                jsonObject.put("city", strCity);
                jsonObject.put("idnumber", id);
                jsonObject.put("gender", utils.getSex(id));
                jsonObject.put("age", String.valueOf(yourAge));

                Log.w("TAG", " jsonObject "+ jsonObject);
                return jsonObject;
            }


        };

        RequestQueue rQueue = Volley.newRequestQueue(this);
        rQueue.add(request);

    }



    /**
     * Represents an asynchronous getSubscriber task used to authenticate
     * the user.
     */
    public void getSubscriberTast(final String phone) {

        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage(getString(R.string.get_subscriber));
        pd.show();

        final StringRequest request = new StringRequest(Request.Method.POST, Constants.GET_SUBSCRIBER_URL, new Response.Listener<String>(){
            @Override
            public void onResponse(String success) {
                Log.w("TAG", " s "+ success);
                if (success != null) try {
                    JSONObject jsonPart = new JSONObject(success);
                    int num = jsonPart.getInt(TAG_SUCCESS);
                    JSONArray jsonArray = jsonPart.getJSONArray(TAG_USER);

                    if (num == 1) {

                        for (int x = 0; x < jsonArray.length(); x++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(x);

                            String name = jsonObject.getString("name");
                            String surname = jsonObject.getString("surname");
                            String phone = jsonObject.getString("phone");
                            String provinc = jsonObject.getString("province");
                            String cit = jsonObject.getString("city");
                            String id = jsonObject.getString("idnumber");
                            String gender = jsonObject.getString("gender");
                            String age = jsonObject.getString("age");

                            subscriber = new Subscriber(name, surname, phone, provinc, cit, id, gender, age);


                                mName.setText(subscriber.getName());
                                mSurname.setText(subscriber.getSurname());
                                mPhone.setText(subscriber.getPhone());
                                etProvince.setText(subscriber.getProvince());
                                etCity.setText(subscriber.getCity());
                                etID.setText(subscriber.getIDnumber());
                                etID.setEnabled(false);
                                etGender.setText(subscriber.getGender());
                                etAge.setText(subscriber.getAge());


                        }


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                pd.dismiss();
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                System.out.println("Erroe" + volleyError);
                utils.toast(volleyError.getMessage());
                pd.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> jsonObject = new HashMap<>();

                jsonObject.put("phone", phone);

                Log.w("TAG", " jsonObject "+ jsonObject);
                return jsonObject;
            }


        };

        RequestQueue rQueue = Volley.newRequestQueue(this);
        rQueue.add(request);

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {



        name = mName.getText().toString();
        if(name.contains(" "))
        {
            mName.setText(name.replace(" ","-"));
            mName.setSelection(name.length());
        }

        surname = mSurname.getText().toString();
        if(surname.contains(" "))
        {
            mSurname.setText(surname.replace(" ","-"));
            mSurname.setSelection(surname.length());
        }

    }


    @Override
    protected void onDestroy() {

        Log.i("MAINACT", "onDestroy!");
        super.onDestroy();

    }


}
