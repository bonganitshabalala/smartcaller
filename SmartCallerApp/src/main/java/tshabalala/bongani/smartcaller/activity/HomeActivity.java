package tshabalala.bongani.smartcaller.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import tshabalala.bongani.smartcaller.R;
import tshabalala.bongani.smartcaller.helper.Constants;
import tshabalala.bongani.smartcaller.helper.Utils;

/**
 * Created by Bongani on 2017/09/11.
 */

public class HomeActivity extends AppCompatActivity {

    //Declared variables
    private EditText etNumber;
    private String phone;
    private String imei;

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private Utils utils;

    @SuppressLint("CommitPrefEdits")
    @Override
    protected void onCreate(Bundle bundleStated) {
        super.onCreate(bundleStated);
        setContentView(R.layout.home_screen);

        //Request permissions once
        ActivityCompat.requestPermissions(HomeActivity.this,
                new String[]{Manifest.permission.READ_PHONE_STATE,Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.CALL_PHONE,
                        Manifest.permission.READ_CONTACTS,Manifest.permission.WRITE_CONTACTS,Manifest.permission.MODIFY_PHONE_STATE,Manifest.permission.SEND_SMS},
                1);
        //Init Helper class
        utils = new Utils(this);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sharedPreferences.edit();

        etNumber = (EditText) findViewById(R.id.editText);


    }

    public void Login(View view)
    {
        attemptRegistration();

    }

    public void signUp(View view)
    {

            startActivity(new Intent(HomeActivity.this,RegisterActivity.class));
            finish();


    }


    private void attemptRegistration() {

        // Store values at the time of the login attempt.

        phone = etNumber.getText().toString();

        boolean cancel = false;
        View focusView = null;


        if (TextUtils.isEmpty(phone)) {
            etNumber.setError("Error - phone can't be empty");
            focusView = etNumber;
            cancel = true;

        }


        if (!TextUtils.isEmpty(phone)) {
            if (!utils.validatePhone(phone)) {
                etNumber.setError("Error - Enter correct cellphone");
                focusView = etNumber;
                cancel = true;

            }
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            //Check if the internet connection
            if (!utils.isOnline(HomeActivity.this)) {
               utils.toast(getString(R.string.no_internet_connection));
            } else {

                 loginTask(phone, imei);

            }
        }
    }


    /**
     * Represents an asynchronous login task used to authenticate
     * the user.
     */
    public void loginTask(final String phonenumber, final String imeinumber) {

        final ProgressDialog pd = new ProgressDialog(HomeActivity.this);
        pd.setMessage(getString(R.string.home_login));
        pd.show();

        final StringRequest request = new StringRequest(Request.Method.POST, Constants.HOME_LOGIN_URL, new Response.Listener<String>(){
            @Override
            public void onResponse(String s) {
                Log.w("TAG", " s "+ s);
                if (!s.equals("null")) {
                    try {
                        JSONObject jsonPart = new JSONObject(s);
                        int num = jsonPart.getInt(TAG_SUCCESS);
                        String message = jsonPart.getString(TAG_MESSAGE);

                        if (num == 1) {

                            if(phone.equals(message)) {

                                editor.putString("phone", phone).commit();
                                Intent intent = new Intent(HomeActivity.this, MainActivity.class);
                                startActivity(intent);
                            }else {

                                utils.toast(getString(R.string.home_error_message)+ message);
                            }


                        } else {
                            utils.toast( message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        utils.toast(getString(R.string.home_error_message));

                    }


                }else {
                    utils.toast(getString(R.string.home_error_message));
                }

                pd.dismiss();
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                System.out.println("Erroe" + volleyError);
                utils.toast(volleyError.getMessage());
                pd.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> jsonObject = new HashMap<>();
                jsonObject.put("phone",phonenumber);
                jsonObject.put("imei",imeinumber);
                Log.w("TAG", " jsonObject "+ jsonObject);
                return jsonObject;
            }


        };

        RequestQueue rQueue = Volley.newRequestQueue(HomeActivity.this);
        rQueue.add(request);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Getting the IMEI number
                    TelephonyManager telephonyManager = ((TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE));
                    imei = telephonyManager.getSimSerialNumber();

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    utils.toast(getString(R.string.permissions_denied));
                    finish();
                }

            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    protected void onDestroy() {
        Log.i("MAINACT", "onDestroy!");
        super.onDestroy();

    }

}
