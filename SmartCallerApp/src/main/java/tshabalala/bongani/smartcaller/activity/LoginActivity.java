package tshabalala.bongani.smartcaller.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import tshabalala.bongani.smartcaller.R;
import tshabalala.bongani.smartcaller.helper.Constants;
import tshabalala.bongani.smartcaller.helper.Utils;


/**
 * Created by Bongani on 2017/08/23.
 */

public class LoginActivity extends AppCompatActivity {

    private EditText mEmailView;
    private EditText mPassword;

    String email;
    String password;


    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;

   Utils utils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.

        utils = new Utils(this);

        mEmailView = (EditText) findViewById(R.id.email);
        mPassword = (EditText) findViewById(R.id.password);

        TextView txtReg = (TextView) findViewById(R.id.textViewRegister);
        txtReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(LoginActivity.this,RegisterActivity.class));
            }
        });

        TextView txtForgot = (TextView) findViewById(R.id.textViewForgot);
        txtForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                utils.toast("Not Implemented");
            }
        });


        Button btnRegister = (Button) findViewById(R.id.button_login);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptRegistration();
            }
        });

    }

    private void attemptRegistration() {

        // Store values at the time of the login attempt.

        email = mEmailView.getText().toString();
        password = mPassword.getText().toString();

        boolean cancel = false;
        View focusView = null;


        if (TextUtils.isEmpty(password)) {
            mPassword.setError("Error - password can't be empty");
            focusView = mPassword;
            cancel = true;

        }


        if (TextUtils.isEmpty(email)) {
            mEmailView.setError("Error - email can't be empty");
            focusView = mEmailView;
            cancel = true;

        }

        if (!TextUtils.isEmpty(email)) {
            if (!utils.isEmailValid(email)) {
                mEmailView.setError("Error - Enter correct email");
                focusView = mEmailView;
                cancel = true;

            }
        }

        if (!TextUtils.isEmpty(password)) {
            if (!utils.isPasswordValid(password)) {
                mPassword.setError("Error - password should be more than 4 digits");
                focusView = mPassword;
                cancel = true;

            }
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            if (!utils.isOnline(LoginActivity.this)) {
                Toast.makeText(LoginActivity.this, "No internet connection", Toast.LENGTH_LONG).show();
            } else {

                 loginTask(email,password);

            }
        }
    }

    /**
     * Represents an asynchronous login task used to authenticate
     * the user.
     */
    public void loginTask(final String phonenumber, final String imeinumber) {

        final ProgressDialog pd = new ProgressDialog(LoginActivity.this);
        pd.setMessage(getString(R.string.home_login));
        pd.show();

        final StringRequest request = new StringRequest(Request.Method.POST, Constants.HOME_LOGIN_URL, new Response.Listener<String>(){
            @Override
            public void onResponse(String success) {
                if (success != null){
                    Log.i("Website Content", success);
                    try {
                        JSONObject jsonPart = new JSONObject(success);
                        int num = jsonPart.getInt(TAG_SUCCESS);
                        String message = jsonPart.getString(TAG_MESSAGE);

                        if (num == 1) {

                            Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                            intent.putExtra("email",email);
                            startActivity(intent);


                        } else {

                            Toast.makeText(LoginActivity.this, message, Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                pd.dismiss();
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                System.out.println("Erroe" + volleyError);
                utils.toast(volleyError.getMessage());
                pd.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> jsonObject = new HashMap<>();
                jsonObject.put("phone",phonenumber);
                jsonObject.put("imei",imeinumber);
                Log.w("TAG", " jsonObject "+ jsonObject);
                return jsonObject;
            }


        };

        RequestQueue rQueue = Volley.newRequestQueue(LoginActivity.this);
        rQueue.add(request);

    }


    @Override
    protected void onDestroy() {

        Log.i("MAINACT", "onDestroy!");
        super.onDestroy();

    }




}

