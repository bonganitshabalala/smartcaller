package tshabalala.bongani.smartcaller.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import tshabalala.bongani.smartcaller.R;
import tshabalala.bongani.smartcaller.adapter.PagerAdapter;
import tshabalala.bongani.smartcaller.fragment.AddContactDialogFragment;
import tshabalala.bongani.smartcaller.fragment.DeactivateDialogFragment;
import tshabalala.bongani.smartcaller.helper.MyContact;
import tshabalala.bongani.smartcaller.helper.Utils.SubcriptionName;

import static tshabalala.bongani.smartcaller.helper.Utils.SubcriptionName.*;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {


    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;
    private String phoneNumber;
    NavigationView navigationView;

    List<MyContact> selectedContact = new ArrayList<>();
    TelephonyManager telephonyManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);
        setSupportActionBar(toolbar);

        SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        phoneNumber = mSharedPreferences.getString("phone", "");

        telephonyManager = ((TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE));

        // receive notifications of telephony state changes
        MyPhoneListener phoneListener = new MyPhoneListener();
        telephonyManager.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //add tabs
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout_main);

        //Set custom layouts to tabs to allow Icons and text
        TabLayout.Tab callsTab = tabLayout.newTab();
        TabLayout.Tab historyTab = tabLayout.newTab();
        TabLayout.Tab blockTab = tabLayout.newTab();
        TabLayout.Tab favouritesTab = tabLayout.newTab();

        tabLayout.addTab(callsTab);
        tabLayout.addTab(historyTab);
        tabLayout.addTab(blockTab);
        tabLayout.addTab(favouritesTab);

        favouritesTab.setCustomView(R.layout.tab_layout_fav);
        callsTab.setCustomView(R.layout.tab_layout_call);
        historyTab.setCustomView(R.layout.tab_layout_hist);
        blockTab.setCustomView(R.layout.tab_layout_bloc);

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        //Viewpager to slide between fragments
        final ViewPager viewPager = (ViewPager)findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter(MainActivity.this, getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setOffscreenPageLimit(2);
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });




    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_add) {

            AddContactDialogFragment addContactDialogFragment = AddContactDialogFragment.newInstance(phoneNumber);
            android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(addContactDialogFragment,"newFragment");
            ft.commit();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_profile) {
            // Handle the camera action
            if(phoneNumber != null) {
                Intent intent = new Intent(this, EditprofileActivity.class);
                intent.putExtra("phone", phoneNumber);
                startActivity(intent);
            }

        } else if (id == R.id.nav_delete) {
            Log.w("TAg"," DeactivateDialogFragment "+ phoneNumber);
            DeactivateDialogFragment deactivateDialogFragment = DeactivateDialogFragment.newInstance(phoneNumber);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(deactivateDialogFragment,"newFragment");
            ft.commit();

        }else if (id == R.id.nav_login) {

            startActivity(new Intent(MainActivity.this,HomeActivity.class));
            finish();

        }else if(id == R.id.nav_balance)
        {
            TelephonyManager  telephonyManager = ((TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE));
            assert telephonyManager != null;
            String  carrierName = telephonyManager.getNetworkOperatorName().trim();
            Log.i("TAG"," operator "+ carrierName);
            SubcriptionName subscriptionName =  valueOfName(carrierName);
            switch (subscriptionName)
            {
                case CELLC:
                    String c_number = "*101"+Uri.encode("#");
                    String c_uri = "tel:" + c_number;
                    Uri c_call = Uri.parse(c_uri);
                    Intent c_intent = new Intent(Intent.ACTION_CALL, c_call);
                    startActivity(c_intent);
                    break;
                case VODACOM:

                    String v_number = "*100"+Uri.encode("#");
                    String v_uri = "tel:" + v_number;
                    Uri v_call = Uri.parse(v_uri);
                    Intent v_intent = new Intent(Intent.ACTION_CALL, v_call);
                    startActivity(v_intent);
                    break;
                case MTN:

                    String m_number = "*141"+Uri.encode("#");
                    String m_uri = "tel:" + m_number;
                    Uri m_call = Uri.parse(m_uri);
                    Intent m_intent = new Intent(Intent.ACTION_CALL, m_call);
                    startActivity(m_intent);

                    break;

            }

        }else if(id == R.id.nav_report)
        {
            startActivity(new Intent(MainActivity.this,ViewReportActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(MainActivity.this, "Permission denied", Toast.LENGTH_SHORT).show();
                }

            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private class MyPhoneListener extends PhoneStateListener {

        private boolean onCall = false;

        @Override

        public void onCallStateChanged(int state, String incomingNumber) {

            switch (state) {

                case TelephonyManager.CALL_STATE_RINGING:
                    // phone ringing...
                    onCall = true;
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:

                    onCall = true;
                    //because user answers the incoming call
                    break;
                case TelephonyManager.CALL_STATE_IDLE:

                    if (onCall) {

                        onCall = false;

                    }

                    break;

                default:

                    break;

            }



        }

    }


}
