package tshabalala.bongani.smartcaller.activity;

import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;

import tshabalala.bongani.smartcaller.R;
import tshabalala.bongani.smartcaller.helper.Utils;

/**
 * Created by Bongani on 2017/08/23.
 */

public class RegisterActivity extends AppCompatActivity implements TextWatcher {

    //Declare variables
    private EditText mName;
    private EditText mSurname;
    private EditText mPhone;

    String surname ;
    String phone;
    String [] province = {"Select Province","Limpopo","North West","Gauteng","Mpumalanga","Free State","KwaZulu-Natal","Northern Cape","Eastern Cape","Western Cape"};
    String [] gautengCities = {"Select City","Johannesburg","Pretoria","Ekuruleni","Sedibeng","Other"};
    String [] limpopoCities = {"Select City","Capricon","Mopani","Sekhukhune","Vhembe","Waterburg","Other"};
    String [] northwestCities = {"Select City","Potchefstroom","Rustenburg","Other"};
    String [] mpumaCities = {"Select City","Nkangala","Gert","Sibande","Enhlanzeni","Other"};
    String [] freeCities = {"Select City","Sasolburg","Welkom","Bloemfontein","Phutaditjhaba","Other"};
    String [] natalCities = {"Select City","Durban","Umhlanga", "New Castle","Margate","Other"};
    String [] nothernCities = {"Select City","Kuruman", "Kimberley","Springbok","Upington","Other"};
    String [] eastCities = {"Select City","East London", "Grahamstown","Mthata","Other"};
    String [] westCities = {"Select City","Cape Town","Stellenbosch", "Hermanues","George","Other"};

    EditText etID, etCity;
    String strProvince,strCity, idNumber;
    private Spinner spinProvince,spinCity;
    String name;
    static int age;
    static int yourAge;
    private Utils utils;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //Init Helper class
        utils = new Utils(this);
        //Init UI elements
        mName = (EditText) findViewById(R.id.name);
        mSurname = (EditText) findViewById(R.id.surname);
        mPhone = (EditText) findViewById(R.id.phone);
        spinProvince = (Spinner) findViewById(R.id.spinnerProvince);
        spinCity = (Spinner) findViewById(R.id.spinnerCity);

        etCity = (EditText) findViewById(R.id.city);
        etID = (EditText) findViewById(R.id.idNumber);

        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<>(this,
                R.layout.spinner_layout, Arrays.asList(province));
        dataAdapter2.setDropDownViewResource(R.layout.spinner_layout);
        spinProvince.setAdapter(dataAdapter2);

        spinProvince.setOnItemSelectedListener(new CustomOnItemSelectedListener());
        spinCity.setOnItemSelectedListener(new CustomOnItemSelectedListeners());


        mName.addTextChangedListener(this);
        mSurname.addTextChangedListener(this);

        Button btnRegister = (Button) findViewById(R.id.button_register);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptRegistration();
            }
        });


    }

    private void attemptRegistration() {

        // Store values at the time of the login attempt.
        name = mName.getText().toString();
        surname = mSurname.getText().toString();
        phone = mPhone.getText().toString();

        String yob = "";
        int y = 0;
        idNumber = etID.getText().toString();


        boolean cancel = false;
        View focusView = null;



        if(spinCity.getVisibility() != View.GONE) {
            strCity = spinCity.getSelectedItem().toString();
            if (strCity.equals("Select.....")) {
                cancel = true;
            }
        }

        if(etCity.getVisibility() != View.GONE) {
            strCity = spinCity.getSelectedItem().toString();
            if (strCity.equals("Other")) {
                strCity = etCity.getText().toString();
            }
        }



        if (TextUtils.isEmpty(idNumber)) {
            etID.setError("Error - ID number can't be empty");
            focusView = etID;
            cancel = true;

        }

        if (!TextUtils.isEmpty(idNumber)) {
            if (!utils.isIDValid(idNumber)) {
                etID.setError("Enter correct ID number");
                focusView = etID;
                cancel = true;

            }
        }

        if (TextUtils.isEmpty(name)) {
            mName.setError("Error - name can't be empty");
            focusView = mName;
            cancel = true;

        }

        if (TextUtils.isEmpty(surname)) {
            mSurname.setError("Error - surname can't be empty");
            focusView = mSurname;
            cancel = true;

        }

        if (TextUtils.isEmpty(phone)) {
            mPhone.setError("Error - cellphone can't be empty");
            focusView = mPhone;
            cancel = true;

        }

        if (!TextUtils.isEmpty(phone)) {
            if (!validatePhone(phone)) {
                mPhone.setError("Error - Enter correct cellphone");
                focusView = mPhone;
                cancel = true;

            }
        }

        if(idNumber != null) {
            try {
                final String year = idNumber.substring(0, 2);
                String month = idNumber.substring(2, 4);
                String day = idNumber.substring(4, 6);

                int yearDate = Integer.parseInt(year);
                int monthDate = Integer.parseInt(month);
                int dayDate = Integer.parseInt(day);

                GregorianCalendar cal = new GregorianCalendar();

                int  m, d, a;

                y = cal.get(Calendar.YEAR);
                m = cal.get(Calendar.MONTH) +1;
                d = cal.get(Calendar.DAY_OF_MONTH);
                cal.set(yearDate, monthDate, dayDate);

                a = y - cal.get(Calendar.YEAR);
                Log.w("TAG", "age "+ a);
                String strAge = String.valueOf(a).substring(2,String.valueOf(a).length());

                 age = Integer.parseInt(strAge);
                Log.w("TAG", "age "+ age);

                if ( age <= 17)
                {

                    focusView = etID;
                    cancel = true;
                    etID.setError("Error - Too young to register");

                }

                String y1,y2,y3c,y4c,y3n4c;
                y1 = "0";
                y2 = "0";


                y3c = String.valueOf(y).valueOf(String.valueOf(y).charAt(0));
                y4c = String.valueOf(y).valueOf(String.valueOf(y).charAt(1));

                y3n4c = y3c +y4c;


                int calcy3c = Integer.parseInt(y3n4c);

                int caly3n = Integer.parseInt(year);

                if(caly3n > calcy3c)
                {
                    y1 = "1";

                    y2 = "9";
                }

                if(caly3n >=0 && caly3n <= calcy3c)
                {
                    y1 = "2";

                    y2 = "0";
                }

                yob = y1 +y2 + year;

                Log.w("HEllo","LEvels "+age+" grtg "+ yearDate + " ddf "+y+" ff "+year+ " fddf "+ yob);




                if (Integer.parseInt(yob) > y)
                {

                    focusView = etID;
                    cancel = true;
                    etID.setError("Error - Future ID number not allowed");

                }




                System.out.println("year is "+ m+" " + monthDate);
                if (monthDate == 0)
                {
                    etID.setError("Error - Enter correct ID number");
                    focusView = etID;
                    cancel = true;
                }
                if(monthDate == 2)
                {
                    if(dayDate == 30 || dayDate ==31)
                    {
                        etID.setError("Error - Enter correct ID number");
                        focusView = etID;
                        cancel = true;
                    }
                }
                System.out.println("year is "+ d+" " + dayDate);
                if (dayDate == 0)
                {
                    etID.setError("Error - Enter correct ID number");
                    focusView = etID;
                    cancel = true;
                }

            }catch (Exception x)
            {
                x.getMessage();
            }
        }

        if (TextUtils.isEmpty(idNumber)) {
            etID.setError("Error - ID number can't be empty");
            focusView = etID;
            cancel = true;

        }

        if (!TextUtils.isEmpty(idNumber)) {
            if (!utils.isIDValid(idNumber)) {
                etID.setError("Enter correct ID number");
                focusView = etID;
                cancel = true;

            }
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            if (!utils.isOnline(RegisterActivity.this)) {
                utils.toast(getString(R.string.no_internet_connection));
            } else {

                yourAge = y - Integer.parseInt(yob);
                Log.w("TAG", " your age "+ yourAge);
                validateFields(yourAge);



            }
        }
    }




    private void validateFields(int yourAge) {

        boolean valid = true;
        if(strProvince.equals("Select Province") || strProvince.equals(""))
        {
            valid = false;
            Toast.makeText(this,"Select Province",Toast.LENGTH_SHORT).show();
        }

        if(spinCity.getVisibility() != View.GONE) {
            if (strCity.equals("Select City") || strCity.equals("")) {
                valid = false;
                Toast.makeText(this, "Select City", Toast.LENGTH_SHORT).show();
            }
            if (strCity.equals("Other")) {
                valid = false;
                etCity.requestFocus();
                Toast.makeText(this, "Enter city name", Toast.LENGTH_SHORT).show();
            }
        }else
        {
            Toast.makeText(this, "Select Province", Toast.LENGTH_SHORT).show();
        }


        if(valid)
        {

            Random random = new Random();
            int number = random.nextInt(9999);
            if(number > 999) {

                String data = "Enter verification code : "+ number;
                utils.sendMySMS(phone,data);
                Intent intent =  new Intent(RegisterActivity.this, VerificationCode.class);
                intent.putExtra("name", name);
                intent.putExtra("surname", surname);
                intent.putExtra("phone", phone);
                intent.putExtra("province", strProvince);
                intent.putExtra("city", strCity);
                intent.putExtra("id", idNumber);
                intent.putExtra("gender", utils.getSex(idNumber));
                intent.putExtra("age", String.valueOf(yourAge));
                intent.putExtra("number",number);
                startActivity(intent);
                Log.w("TAG","code " +number);
                finish();
            }
        }


    }


    public  boolean validatePhone(String phoneNo) {
        //validate phone numbers of format "1234567890"
        boolean valid ;
        String preg = "((?:\\+27|27)|0)(=99|72|82|73|83|74|84|86|79|81|71|76|60|61|62|63|64|78|)(\\d{7})";

        valid = Pattern.matches(preg, phoneNo);
        return valid;

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {


        name = mName.getText().toString();
        if(name.contains(" "))
        {
            mName.setText(name.replace(" ","-"));
            mName.setSelection(name.length());
        }

        surname = mSurname.getText().toString();
        if(surname.contains(" "))
        {
            mSurname.setText(surname.replace(" ","-"));
            mSurname.setSelection(surname.length());
        }

    }


    @Override
    protected void onDestroy() {
        //  stopService(mServiceIntent);
        Log.i("MAINACT", "onDestroy!");
        super.onDestroy();

    }

    public class CustomOnItemSelectedListeners implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

            strCity = spinCity.getSelectedItem().toString();
            if (strCity.equals("Select City")) {
                etCity.setVisibility(View.GONE);
                // Toast.makeText(AddressActivity.this, "Please select city", Toast.LENGTH_SHORT).show();
            } else if (strCity.equals("Other")) {

                etCity.setVisibility(View.VISIBLE);
                strCity = etCity.getText().toString();

            }else
            {
                etCity.setVisibility(View.GONE);
            }

        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    public class CustomOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

            strProvince = spinProvince.getSelectedItem().toString();
            if (strProvince.equals("Select Province")) {
                spinCity.setVisibility(View.GONE);
                etCity.setVisibility(View.GONE);
            } else {
                spinCity.setVisibility(View.VISIBLE);

                if (strProvince.equals("Limpopo")) {
                    ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(RegisterActivity.this,
                            R.layout.spinner_layout, Arrays.asList(limpopoCities));
                    dataAdapter1.setDropDownViewResource(R.layout.spinner_layout);
                    spinCity.setAdapter(dataAdapter1);


                } else if (strProvince.equals("Gauteng")) {
                    ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(RegisterActivity.this,
                            R.layout.spinner_layout, Arrays.asList(gautengCities));
                    dataAdapter1.setDropDownViewResource(R.layout.spinner_layout);
                    spinCity.setAdapter(dataAdapter1);



                } else if (strProvince.equals("North West")) {
                    ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(RegisterActivity.this,
                            R.layout.spinner_layout, Arrays.asList(northwestCities));
                    dataAdapter1.setDropDownViewResource(R.layout.spinner_layout);
                    spinCity.setAdapter(dataAdapter1);


                } else if (strProvince.equals("Mpumalanga")) {
                    ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(RegisterActivity.this,
                            R.layout.spinner_layout, Arrays.asList(mpumaCities));
                    dataAdapter1.setDropDownViewResource(R.layout.spinner_layout);
                    spinCity.setAdapter(dataAdapter1);



                } else if (strProvince.equals("Free State")) {

                    ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(RegisterActivity.this,
                            R.layout.spinner_layout, Arrays.asList(freeCities));
                    dataAdapter1.setDropDownViewResource(R.layout.spinner_layout);
                    spinCity.setAdapter(dataAdapter1);


                } else if (strProvince.equals("KwaZulu-Natal")) {
                    ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(RegisterActivity.this,
                            R.layout.spinner_layout, Arrays.asList(natalCities));
                    dataAdapter1.setDropDownViewResource(R.layout.spinner_layout);
                    spinCity.setAdapter(dataAdapter1);

                } else if (strProvince.equals("Northern Cape")) {
                    ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(RegisterActivity.this,
                            R.layout.spinner_layout, Arrays.asList(nothernCities));
                    dataAdapter1.setDropDownViewResource(R.layout.spinner_layout);
                    spinCity.setAdapter(dataAdapter1);

                } else if (strProvince.equals("Eastern Cape")) {
                    ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(RegisterActivity.this,
                            R.layout.spinner_layout, Arrays.asList(eastCities));
                    dataAdapter1.setDropDownViewResource(R.layout.spinner_layout);
                    spinCity.setAdapter(dataAdapter1);


                } else if (strProvince.equals("Western Cape")) {
                    ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(RegisterActivity.this,
                            R.layout.spinner_layout, Arrays.asList(westCities));
                    dataAdapter1.setDropDownViewResource(R.layout.spinner_layout);
                    spinCity.setAdapter(dataAdapter1);


                }


            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
            // TODO Auto-generated method stub
        }

    }



}
