package tshabalala.bongani.smartcaller.activity;

import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tshabalala.bongani.smartcaller.R;
import tshabalala.bongani.smartcaller.helper.Constants;
import tshabalala.bongani.smartcaller.helper.Utils;

/**
 * Created by Bongani on 2017/09/17.
 */

public class VerificationCode extends AppCompatActivity implements TextWatcher{

    private ProgressDialog pDialog;

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;
    private EditText etCode;
    private Button btnProcess;

    String code;
    String name;
    String surname;
    String strProvince;
    String phone;
    String city;
    String idNumber;
    String gender;
    String age;
    int number;
    private Utils utils;

    @Override
    protected void onCreate(Bundle savedBundleState)
    {
        super.onCreate(savedBundleState);
        setContentView(R.layout.one_time_pin);

        //Init Helper class
        utils = new Utils(this);

        //Init UI elements
        etCode = (EditText)findViewById(R.id.code);
        etCode.addTextChangedListener(this);

        name = getIntent().getStringExtra("name");
        surname = getIntent().getStringExtra("surname");
        phone = getIntent().getStringExtra("phone");
        strProvince = getIntent().getStringExtra("province");
        city = getIntent().getStringExtra("city");
        idNumber = getIntent().getStringExtra("id");
        gender = getIntent().getStringExtra("gender");
        age = getIntent().getStringExtra("age");
        number = getIntent().getIntExtra("number",0);
        Log.i("TAG"," code "+ number);

        btnProcess = (Button)findViewById(R.id.button_process);
        btnProcess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                verifyCode();
            }
        });


    }

    private void verifyCode() {

        code = etCode.getText().toString();
        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(code)) {
            etCode.setError("Error - name can't be empty");
            focusView = etCode;
            cancel = true;

        }

        if (!TextUtils.isEmpty(code)) {
            if(Integer.parseInt(code) != number)
            {
                etCode.setError("Error - verification code don't match");
                focusView = etCode;
                cancel = true;
            }


        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.

            registrationTask();



        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

        String code = etCode.getText().toString();
        if(Integer.parseInt(code) != number)
        {
            etCode.setError("Error - verification code don't match");
            etCode.requestFocus();
        }
    }


    /**
     * Represents an asynchronous register task used to authenticate
     * the user.
     */
    public void registrationTask() {

        final ProgressDialog pd = new ProgressDialog(VerificationCode.this);
        pd.setMessage(getString(R.string.register));
        pd.show();

        final StringRequest request = new StringRequest(Request.Method.POST, Constants.VERIFICATION_URL, new Response.Listener<String>(){
            @Override
            public void onResponse(String s) {
                Log.w("TAG", " s "+ s);
                if (s != null){

                    try {
                        JSONObject jsonPart = new JSONObject(s);
                        int num = jsonPart.getInt(TAG_SUCCESS);
                        String message = jsonPart.getString(TAG_MESSAGE);

                        if (num == 1) {

                            SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(VerificationCode.this);
                            SharedPreferences.Editor editor = mPrefs.edit();
                            editor.putString("phone", phone).commit();;

                            Intent intent =  new Intent(VerificationCode.this, MainActivity.class);
                            intent.putExtra("number",number);
                            startActivity(intent);
                            finish();



                        } else {
                            // pDialog.dismiss();
                            Toast.makeText(VerificationCode.this, message, Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                pd.dismiss();
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                System.out.println("Erroe" + volleyError);
                utils.toast(volleyError.getMessage());
                pd.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> jsonObject = new HashMap<>();
                TelephonyManager telephonyManager = ((TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE));

                String imei =  telephonyManager.getSimSerialNumber();

                        jsonObject.put("name", name);
                        jsonObject.put("surname", surname);
                        jsonObject.put("phone", phone);
                        jsonObject.put("province", strProvince);
                        jsonObject.put("city", city);
                        jsonObject.put("idnumber", idNumber);
                        jsonObject.put("gender", gender);
                        jsonObject.put("age", age);
                        jsonObject.put("imei", imei);
                Log.w("TAG", " jsonObject "+ jsonObject);
                return jsonObject;
            }


        };

        RequestQueue rQueue = Volley.newRequestQueue(VerificationCode.this);
        rQueue.add(request);

    }
}
