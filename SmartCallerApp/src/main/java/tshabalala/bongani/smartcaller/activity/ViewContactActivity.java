package tshabalala.bongani.smartcaller.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import tshabalala.bongani.smartcaller.R;
import tshabalala.bongani.smartcaller.fragment.ContactDialogFragment;
import tshabalala.bongani.smartcaller.fragment.PhoneContactDialogFragment;
import tshabalala.bongani.smartcaller.helper.Constants;
import tshabalala.bongani.smartcaller.helper.MyContact;
import tshabalala.bongani.smartcaller.helper.Utils;

/**
 * Created by Bongani on 2017/10/11.
 */

public class ViewContactActivity extends AppCompatActivity {

    MyContact contact;
    TextView txtname;
    CheckBox chFav;
    Button btnBack;
    EditText etNumber,etNumberTwo;
    String subscriberphone;

    ImageButton img,imgTwo;

    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;

    private static final String TAG_SUCCESS = "success";
    Utils utils;


    @Override
    protected void onCreate(Bundle bundlleStateSaved)
    {
        super.onCreate(bundlleStateSaved);
        setContentView(R.layout.phone_number);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("View contact");
        setSupportActionBar(toolbar);

        utils = new Utils(this);

        txtname = (TextView)findViewById(R.id.textName);
        chFav = (CheckBox)findViewById(R.id.checkFav);
        etNumber = (EditText) findViewById(R.id.phone);
        etNumber.setEnabled(false);
        etNumberTwo = (EditText) findViewById(R.id.phonetwo);
        etNumberTwo.setEnabled(false);
        img = (ImageButton)findViewById(R.id.imageButtonCall);
        imgTwo = (ImageButton)findViewById(R.id.imageButtonCallTwo);

        contact = (MyContact) getIntent().getSerializableExtra("contact");
        subscriberphone = getIntent().getStringExtra("sub");

        txtname.setText(contact.getName());

        if(contact.getFavourite().equals("Yes"))
        {
            chFav.setChecked(true);
        }

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ContactDialogFragment logOptionsDialogFragment = ContactDialogFragment.newInstance(contact.getPhone(),contact.getName(),subscriberphone);
                android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.add(logOptionsDialogFragment,"newFragment");
                ft.commit();

            }
        });

        imgTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ContactDialogFragment logOptionsDialogFragment = ContactDialogFragment.newInstance(etNumberTwo.getText().toString(), contact.getName(), subscriberphone);
                android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.add(logOptionsDialogFragment,"newFragment");
                ft.commit();

            }
        });


        etNumber.setText(contact.getPhone());


        if(!contact.getPhoneTwo().equals(""))
        {
            etNumberTwo.setVisibility(View.VISIBLE);
            imgTwo.setVisibility(View.VISIBLE);
            etNumberTwo.setText(contact.getPhoneTwo());
        }else
        {
            etNumberTwo.setVisibility(View.INVISIBLE);
            imgTwo.setVisibility(View.INVISIBLE);

        }
        btnBack = (Button)findViewById(R.id.buttonBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });




    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.view_phone_menu, menu);
        return true;
    }

    @SuppressLint("CommitTransaction")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_add:

                PhoneContactDialogFragment logOptionsDialogFragment = PhoneContactDialogFragment.newInstance(subscriberphone,contact,chFav.isChecked());
                android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.add(logOptionsDialogFragment,"newFragment");

                break;
            case R.id.action_del:
                DeleteContactsTast(subscriberphone,contact.getName());
                break;

            default:
                break;
        }

        return false;
    }

    /**
     * Represents an asynchronous DeleteContact task used to authenticate
     * the user.
     */
    public void DeleteContactsTast(final String subscriberphone,final String name) {

        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage(getString(R.string.delete_contact));
        pd.show();

        final StringRequest request = new StringRequest(Request.Method.POST, Constants.DELETE_CONTACT_URL, new Response.Listener<String>(){
            @Override
            public void onResponse(String success) {
                if (success != null){
                    Log.i("Hello",success);
                    try {

                        JSONObject jsonPart = new JSONObject(success);
                        int num = jsonPart.getInt(TAG_SUCCESS);
                        String message = jsonPart.getString(TAG_SUCCESS);

                        if (num == 1) {

                            utils.toast("Contact successfully deleted");

                            startActivity(new Intent(ViewContactActivity.this,MainActivity.class));
                            finish();

                        } else {
                            utils.toast(message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                pd.dismiss();
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                System.out.println("Erroe" + volleyError);
                utils.toast(volleyError.getMessage());
                pd.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> jsonObject = new HashMap<>();
                jsonObject.put("phone", subscriberphone);
                jsonObject.put("name", name);

                Log.w("TAG", " jsonObject "+ jsonObject);
                return jsonObject;
            }


        };

        RequestQueue rQueue = Volley.newRequestQueue(this);
        rQueue.add(request);

    }

}
