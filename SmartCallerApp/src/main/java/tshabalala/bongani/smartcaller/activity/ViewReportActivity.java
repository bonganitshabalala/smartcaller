package tshabalala.bongani.smartcaller.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.opencsv.CSVWriter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import tshabalala.bongani.smartcaller.R;
import tshabalala.bongani.smartcaller.adapter.LogReportAdapter;
import tshabalala.bongani.smartcaller.helper.Constants;
import tshabalala.bongani.smartcaller.helper.FileDialog;
import tshabalala.bongani.smartcaller.helper.History;
import tshabalala.bongani.smartcaller.helper.Utils;

/**
 * Created by Bongani on 2017/10/20.
 */

public class ViewReportActivity extends AppCompatActivity {


    private static final String TAG_SUCCESS = "success";
    private static final String TAG_USER = "history";
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;
    List<History> histories = new ArrayList<>();
    List<History> rawList = new ArrayList<>();
    TableLayout checkColumn;
    TableRow rowSheet;

    private static final String logDir = "/LogData";
    private static final String logFile = "/SmartCallerReport";
    File sdcFile;
    List<String> timeList = new ArrayList<>();
    Spinner spinTime;
    Utils utils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            builder.detectFileUriExposure();
        }
        utils = new Utils(this);


        SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        String phoneNumber = mSharedPreferences.getString("phone", "");

        GetCallHistoryTast(phoneNumber);

        spinTime = (Spinner)findViewById(R.id.spinnerTime);


        final Button btn = (Button) findViewById(R.id.buttonEdit);


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(ViewReportActivity.this,MainActivity.class);
                startActivity(i);

            }
        });

        final Button btnSave = (Button) findViewById(R.id.buttonSave);


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                makeLog();

            }
        });

        final Button btnView = (Button) findViewById(R.id.buttonView);


        btnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Utils.isExternalStorageAvailable() || !Utils.isExternalStorageReadOnly()) {

                    sdcFile = new File(Environment.getExternalStorageDirectory()+ logDir);
                    if(sdcFile.exists()) {
                        FileDialog fileDialog = new FileDialog(ViewReportActivity.this, sdcFile);
                        fileDialog.addFileListener(new FileDialog.FileSelectedListener() {
                            public void fileSelected(File file) {
                                Log.d(getClass().getName(), "selected file " + file.getName());

                                Intent intent = new Intent();
                                intent.setAction(android.content.Intent.ACTION_VIEW);
                                intent.setDataAndType(Uri.fromFile(file),getMimeType(file.getAbsolutePath()));
                                startActivity(intent);

                            }
                        });
                        fileDialog.showDialog();
                    }

                }


            }
        });



    }



    private String getMimeType(String url)
    {
        String parts[]=url.split("\\.");
        String extension=parts[parts.length-1];
        String type = null;
        if (extension != null) {
            MimeTypeMap mime = MimeTypeMap.getSingleton();
            type = mime.getMimeTypeFromExtension(extension);
        }
        return type;
    }


    /**
     * Represents an asynchronous getCallLogs task used to authenticate
     * the user.
     */
    public void GetCallHistoryTast(final String phone) {

        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage(getString(R.string.get_history_log));
        pd.show();

        final StringRequest request = new StringRequest(Request.Method.POST, Constants.GET_HISTORY_URL, new Response.Listener<String>(){
            @Override
            public void onResponse(String success) {
                if (success != null){
                    Log.i("Website Content", success);
                    try {
                        JSONObject jsonPart = new JSONObject(success);
                        int num = jsonPart.getInt(TAG_SUCCESS);
                        JSONArray jsonArray = jsonPart.getJSONArray(TAG_USER);

                        if (num == 1) {

                            for(int x =0 ; x < jsonArray.length(); x++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(x);

                                String name = jsonObject.getString("name");
                                String phone = jsonObject.getString("phone");
                                String date = jsonObject.getString("date");
                                String time = jsonObject.getString("time");
                                String duration = jsonObject.getString("duration");
                                History myContact = new History(name,phone,date,time,duration);

                                if (!histories.contains(myContact)) {
                                    histories.add(myContact);
                                }

                                if (!timeList.contains(myContact.getTime())) {
                                    timeList.add(myContact.getTime());
                                }


                            }

                            ArrayAdapter<String> dataAdapter3 = new ArrayAdapter<>(ViewReportActivity.this,
                                    R.layout.spinner_report_view,timeList);
                            dataAdapter3.setDropDownViewResource(R.layout.spinner_layout);
                            spinTime.setAdapter(dataAdapter3);

                            spinTime.setOnItemSelectedListener(new CustomOnItemSelectedListenerrrr());

                            display(histories);


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                pd.dismiss();
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                System.out.println("Erroe" + volleyError);
                utils.toast(volleyError.getMessage());
                pd.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> jsonObject = new HashMap<>();

                jsonObject.put("subscriberphone", phone);

                Log.w("TAG", " jsonObject "+ jsonObject);
                return jsonObject;
            }


        };

        RequestQueue rQueue = Volley.newRequestQueue(this);
        rQueue.add(request);

    }


    public TextView makeTableRowWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels) {
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        TextView textView = new TextView(this);
        textView.setText(text);
        textView.setTextColor(Color.BLACK);
        textView.setTextSize(16);
        textView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        textView.setHeight(fixedHeightInPixels);
        return textView;
    }

    public TextView makeTableHeader(final String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels,final List<History> displayListList) {

        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        final TextView textView = new TextView(this);
        textView.setText(text);
        textView.setTextColor(Color.BLUE);
        textView.setTextSize(10);
        textView.setBackgroundColor(getResources().getColor(R.color.color8));
        textView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        textView.setHeight(fixedHeightInPixels);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(text.equals("Name"))
                {

                    Collections.sort(displayListList, new Comparator<History>() {
                        public int compare(History obj1, History obj2) {
                            // ## Ascending order
                            return obj2.getName().compareToIgnoreCase(obj1.getName());
                            // return Integer.valueOf(obj1.getId).compareTo(obj2.getId); // To compare integer values

                        }
                    });


                }
                if(text.equals("Date"))
                {   System.out.println("select name :"+text);
                    Collections.sort(displayListList, new Comparator<History>() {
                        public int compare(History obj1, History obj2) {
                            // ## Ascending order
                            return obj2.getDate().compareTo(obj1.getDate());

                        }
                    });

                }

                //  textToSort(textView,text);
                display(displayListList);
            }
        });

        textView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                if(text.equals("Name")) {
                    Collections.sort(displayListList, new Comparator<History>() {
                        public int compare(History obj1, History obj2) {
                            // ## Ascending order
                            return obj1.getName().compareToIgnoreCase(obj2.getName());
                       }
                    });


                }else if(text.equals("Date"))
                {
                    Collections.sort(displayListList, new Comparator<History>() {
                        public int compare(History obj1, History obj2) {
                            // ## Ascending order
                            return obj1.getDate().compareToIgnoreCase(obj2.getDate());
                            // return Integer.valueOf(obj1.getId).compareTo(obj2.getId); // To compare integer values

                        }
                    });
                }
                display(displayListList);
                return false;

            }
        });
        return textView;
    }

    public void display(List<History> displayListList) {

        if(displayListList != null)
        {

            int fixedRowHeight = 50;
            int fixedHeaderHeight = 60;

            rowSheet = new TableRow(this);
            //header (fixed vertically)
            TableRow.LayoutParams wrapWrapTableRowSheetParams = new TableRow.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT);
            int[] fixedColumnWidthsSheet = new int[]{ 20, 15, 20, 10, 10};
            int[] scrollableColumnWidthsSheet = new int[]{ 20, 15, 20, 10, 15};

            //header (fixed vertically)
            TableLayout header = (TableLayout) findViewById(R.id.table_header);
            header.removeAllViews();
            rowSheet.setLayoutParams(wrapWrapTableRowSheetParams);
            rowSheet.setGravity(Gravity.CENTER);
            // rowSheet.addView(makeTableHeader("NO", fixedColumnWidthsSheet[0], fixedHeaderHeight, displayListList));
            rowSheet.addView(makeTableHeader("Name", fixedColumnWidthsSheet[0], fixedHeaderHeight, displayListList));
            rowSheet.addView(makeTableHeader("Phone", fixedColumnWidthsSheet[1], fixedHeaderHeight, displayListList));
            rowSheet.addView(makeTableHeader("Date", fixedColumnWidthsSheet[2], fixedHeaderHeight, displayListList));
            rowSheet.addView(makeTableHeader("Time", fixedColumnWidthsSheet[3], fixedHeaderHeight, displayListList));
            rowSheet.addView(makeTableHeader("Duration", fixedColumnWidthsSheet[4], fixedHeaderHeight, displayListList));

            header.addView(rowSheet);


            //header for checkbox
            checkColumn = (TableLayout) findViewById(R.id.check_column);
            //header (fixed horizontally)
            TableLayout fixedColumn = (TableLayout) findViewById(R.id.fixed_column);
            fixedColumn.removeAllViews();
            //rest of the table (within a scroll view)
            TableLayout scrollablePart = (TableLayout) findViewById(R.id.scrollable_part);
            scrollablePart.removeAllViews();

            for (History timeslot : displayListList) {

                rawList.add(timeslot);

                rowSheet = new TableRow(this);

                rowSheet.setLayoutParams(wrapWrapTableRowSheetParams);
                rowSheet.setGravity(Gravity.CENTER);
                rowSheet.setBackgroundColor(Color.WHITE);
                rowSheet.addView(makeTableRowWithText("" + timeslot.getName(), scrollableColumnWidthsSheet[0], fixedRowHeight));
                rowSheet.addView(makeTableRowWithText("" + timeslot.getPhone(), scrollableColumnWidthsSheet[1], fixedRowHeight));
                rowSheet.addView(makeTableRowWithText("" + timeslot.getDate(), scrollableColumnWidthsSheet[2], fixedRowHeight));
                rowSheet.addView(makeTableRowWithText("" + timeslot.getTime(), scrollableColumnWidthsSheet[3], fixedRowHeight));

                rowSheet.addView(makeTableRowWithText("" + ((Integer.parseInt(timeslot.getDuration())/60)+" mins" +" "+ (Integer.parseInt(timeslot.getDuration())% 60)+" secs"), scrollableColumnWidthsSheet[4], fixedRowHeight));
                scrollablePart.addView(rowSheet);


            }

        }


    }

    public void makeLog() {
        try {

            final ListView listView = new ListView(this);
            listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);

            final LogReportAdapter logAdapter = new LogReportAdapter(this, R.id.textView, rawList);
            listView.setAdapter(logAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    logAdapter.notifyDataSetChanged();
                }
            });

            AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyWhiteAlertDialogStyle);
            builder.setTitle("Activities Log - ");
            builder.setView(listView)
                    .setNeutralButton("Store Log", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Write Raw Frame Data to File

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        boolean success;
                                        File dir = new File(Environment.getExternalStorageDirectory()+ logDir);

                                        success = (dir.mkdir() || dir.isDirectory());
                                        if (success) {
                                            String timelabel = "_" + Utils.getDateTime(new Date()).replace(" ", "_").replace(":", "h").replace("/","-");
                                            CSVWriter writer = new CSVWriter(new FileWriter(dir+ logFile + timelabel+".csv"), ',');
                                       for (ListIterator<History> i = histories.listIterator(); i.hasNext(); ) {
                                                History entry = i.next();
                                                String[] entries = (entry.getName() + " : "+ entry.getPhone()+" : "+entry.getDate() +" : "+entry.getTime()).split(":");
                                                writer.writeNext(entries);
                                            }

                                            writer.close();
                                            Toast.makeText(ViewReportActivity.this,"Successfully created file",Toast.LENGTH_SHORT).show();

//                                            }
                                        } else {
                                            Toast.makeText(ViewReportActivity.this,"Could not create directory \"/LogData\"",Toast.LENGTH_SHORT).show();
                                        }

                                        //printLog("Log written to file...");
                                    } catch (Exception e) {
                                        e.getStackTrace();
                                    }
                                }
                            }, "writeFileThread").start();

                        }
                    })
                    .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (dialog != null)
                                dialog.dismiss();
                        }
                    });
            AlertDialog dialog = builder.create();
            dialog.show();
            listView.smoothScrollToPosition(logAdapter.getCount()-1);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        } catch (Exception e){
            e.getStackTrace();
        }
    }

    public class CustomOnItemSelectedListenerrrr implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {


          String  time =  spinTime.getSelectedItem().toString();


            List<History> result = new ArrayList<>();


            for (History person: histories) {
                if (person.getTime().equals(time)) {
                    result.add(person);
                }
            }

            display(result);

        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }
}
