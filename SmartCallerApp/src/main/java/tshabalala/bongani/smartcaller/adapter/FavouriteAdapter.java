package tshabalala.bongani.smartcaller.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import tshabalala.bongani.smartcaller.R;
import tshabalala.bongani.smartcaller.helper.MyContact;

/**
 * Created by Bongani on 2017/09/25.
 */


public class FavouriteAdapter extends ArrayAdapter<MyContact> {

    private Activity mContext;
    private LayoutInflater mInflater;

    private static class ViewHolder {
        private TextView mSideBarText = null;
    }

    public FavouriteAdapter(Activity context, int textViewResourceId, List<MyContact> objects){
        super(context, textViewResourceId, objects);
        this.mContext = context;
        mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @NonNull
    @Override
    public View getView(int position, View contentView, final ViewGroup parent){
        final ViewHolder vh;
        final MyContact obj = getItem(position);
        if(contentView == null){
            vh = new ViewHolder();
            contentView = mInflater.inflate(R.layout.sidebar_listview, parent, false);

            vh.mSideBarText = (TextView) contentView.findViewById(R.id.sideBarText);
            contentView.setTag(vh);
        } else {
            vh = (ViewHolder) contentView.getTag();
        }

        vh.mSideBarText.setText(obj.getName() + " : "+obj.getPhone());

        return (contentView);
    }
}
