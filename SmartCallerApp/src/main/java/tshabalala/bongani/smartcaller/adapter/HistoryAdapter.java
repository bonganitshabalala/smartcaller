package tshabalala.bongani.smartcaller.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import tshabalala.bongani.smartcaller.R;
import tshabalala.bongani.smartcaller.helper.History;

/**
 * Created by Bongani on 2017/09/26.
 */

public class HistoryAdapter  extends ArrayAdapter<History> {

    private Activity mContext;
    private LayoutInflater mInflater;

    private static class ViewHolder {
        private TextView mSideBarName = null;
        private TextView mSideBarPhone = null;
        private TextView mSideBarDate = null;
        private TextView mSideBarTime = null;
    }

    public HistoryAdapter(Activity context, int textViewResourceId, List<History> objects){
        super(context, textViewResourceId, objects);
        this.mContext = context;
        mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @NonNull
    @Override
    public View getView(int position, View contentView, final ViewGroup parent){
        final ViewHolder vh;
        final History obj = getItem(position);
        if(contentView == null){
            vh = new ViewHolder();
            contentView = mInflater.inflate(R.layout.sidebar_history, parent, false);

            vh.mSideBarName = (TextView) contentView.findViewById(R.id.name);
            vh.mSideBarPhone = (TextView) contentView.findViewById(R.id.phone);
            vh.mSideBarDate = (TextView) contentView.findViewById(R.id.date);
            vh.mSideBarTime = (TextView) contentView.findViewById(R.id.time);
            contentView.setTag(vh);
        } else {
            vh = (ViewHolder) contentView.getTag();


        }

        vh.mSideBarName.setText(obj.getName());
        vh.mSideBarPhone.setText(obj.getPhone());
        vh.mSideBarDate.setText(obj.getDate());
        vh.mSideBarTime.setText(obj.getTime());



        return (contentView);
    }
}

