package tshabalala.bongani.smartcaller.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import tshabalala.bongani.smartcaller.R;
import tshabalala.bongani.smartcaller.helper.MyContact;

/**
 * Created by Bongani on 2017/10/10.
 */

public class ListViewAdapter extends BaseAdapter {

    // Declare Variables

    private Context mContext;
    private LayoutInflater inflater;
    private List<MyContact> contactList;
    private ArrayList<MyContact> arraylist;

    public ListViewAdapter(Context context, List<MyContact> contactList) {
        mContext = context;
        this.contactList = contactList;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<>();
        this.arraylist.addAll(contactList);
    }

    public class ViewHolder {
        TextView name;
        TextView phone;
    }

    @Override
    public int getCount() {
        return contactList.size();
    }

    @Override
    public MyContact getItem(int position) {
        return contactList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.listview_item, null);
            // Locate the TextViews in listview_item.xml
            holder.name = (TextView) view.findViewById(R.id.name);
            holder.phone = (TextView) view.findViewById(R.id.phone);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        // Set the results into TextViews
        holder.name.setText(contactList.get(position).getName());
        holder.phone.setText(contactList.get(position).getPhone());
        return view;
    }

    // Filter Class
    public void filter(String charText) {

        try {
            if(charText != null) {
                charText = charText.toLowerCase(Locale.getDefault());
                contactList.clear();
                if (charText.length() == 0) {
                    contactList.addAll(arraylist);
                } else {
                    for (MyContact wp : arraylist) {
                        if (wp.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                            contactList.add(wp);
                        }
                    }
                }
                notifyDataSetChanged();
            }
        }catch (Exception x)
        {
            x.getMessage();
        }
    }

}