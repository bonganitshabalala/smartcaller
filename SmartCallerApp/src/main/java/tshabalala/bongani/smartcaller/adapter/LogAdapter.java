package tshabalala.bongani.smartcaller.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import tshabalala.bongani.smartcaller.R;
import tshabalala.bongani.smartcaller.helper.MyContact;

/**
 * Created by Bongani on 2017/11/24.
 */

public class LogAdapter extends ArrayAdapter<MyContact> {

    private Activity mContext;
    private LayoutInflater mInflater;

    private static class ViewHolder {
        private TextView tvName = null;
        private TextView tvPhone = null;


    }

    public LogAdapter(Activity context, int textViewResourceId, List<MyContact> objects){
        super(context, textViewResourceId, objects);
        this.mContext = context;
        mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View contentView, final ViewGroup parent){
        final MyContact item = getItem(position);
        final ViewHolder vh;
        if(contentView == null){
            vh = new ViewHolder();
            contentView = mInflater.inflate(R.layout.logs, parent, false);


            vh.tvName = (TextView) contentView.findViewById(R.id.name );
            vh.tvPhone = (TextView) contentView.findViewById(R.id.phone);



            contentView.setTag(vh);
        } else {
            vh = (ViewHolder) contentView.getTag();
        }
        if(item.getFavourite().equals("Yes")) {
            vh.tvName.setText(item.getName());
            vh.tvPhone.setText(item.getPhone());
        }else
        {
            vh.tvName.setText("");
            vh.tvPhone.setText("");
        }


        return (contentView);
    }
}
