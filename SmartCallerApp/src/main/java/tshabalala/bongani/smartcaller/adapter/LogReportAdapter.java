package tshabalala.bongani.smartcaller.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import tshabalala.bongani.smartcaller.R;
import tshabalala.bongani.smartcaller.helper.History;


/**
 * Created by Bongani on 2017/11/24.
 */


public class LogReportAdapter extends ArrayAdapter<History> {

    private Activity mContext ;
    private LayoutInflater mInflater;

    private static class ViewHolder {
        private TextView tvName = null;
        private TextView tvPhone = null;
        private TextView tvDate = null;
        private TextView tvTime = null;


    }

    public LogReportAdapter(Activity context, int textViewResourceId, List<History> objects){
        super(context, textViewResourceId, objects);
        this.mContext = context;
        mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View contentView, final ViewGroup parent){
        final History item = getItem(position);
        final ViewHolder vh;
        if(contentView == null){
            vh = new ViewHolder();
            contentView = mInflater.inflate(R.layout.logreport, parent, false);


            vh.tvName = (TextView) contentView.findViewById(R.id.name );
            vh.tvPhone = (TextView) contentView.findViewById(R.id.phone);
            vh.tvDate = (TextView) contentView.findViewById(R.id.date );
            vh.tvTime = (TextView) contentView.findViewById(R.id.time);



            contentView.setTag(vh);
        } else {
            vh = (ViewHolder) contentView.getTag();
        }
        vh.tvName.setText(item.getName());
        vh.tvPhone.setText(item.getPhone());
        vh.tvDate.setText(item.getDate());
        vh.tvTime.setText(item.getTime());


        return (contentView);
    }
}

