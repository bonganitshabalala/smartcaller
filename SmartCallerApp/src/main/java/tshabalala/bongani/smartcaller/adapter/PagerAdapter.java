package tshabalala.bongani.smartcaller.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import tshabalala.bongani.smartcaller.fragment.BlockFragment;
import tshabalala.bongani.smartcaller.fragment.CallFragment;
import tshabalala.bongani.smartcaller.fragment.FavouriteFragment;
import tshabalala.bongani.smartcaller.fragment.HistoryFragment;

/**
 * This Pager adapter is used to attach the tabs to the tablayout in the main activity.
 */
public class PagerAdapter extends FragmentStatePagerAdapter {

    private int mNumOfTabs;

    public PagerAdapter(Context context, FragmentManager fm , int NumOfTabs)
    {
        super(fm);
        Context context1 = context;
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch(position)
        {
            case 0:
                return new CallFragment();
            case 1:
                return new HistoryFragment();
            case 2:
                return new BlockFragment();
            case 3:
                return new FavouriteFragment();
            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

}
