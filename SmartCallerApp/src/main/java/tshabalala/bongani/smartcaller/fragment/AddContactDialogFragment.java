package tshabalala.bongani.smartcaller.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tshabalala.bongani.smartcaller.R;
import tshabalala.bongani.smartcaller.activity.MainActivity;
import tshabalala.bongani.smartcaller.helper.Constants;
import tshabalala.bongani.smartcaller.helper.MyContact;
import tshabalala.bongani.smartcaller.helper.Utils;


/**
 * Created by Bongani on 2017/09/22.
 */

public class AddContactDialogFragment extends DialogFragment implements TextWatcher{

    private static final String TAG_USER = "con";
    private static String phoneNumber;
    private EditText etPhone,etName;
    private String name,phone;
    private CheckBox checkFav;
    String favourite = "";

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";

     List<MyContact> selectedContact = new ArrayList<>();
     private Utils utils;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);

    }

    public static AddContactDialogFragment newInstance(String phone) {

        Bundle bundle = new Bundle();
        phoneNumber = phone;

        AddContactDialogFragment sampleFragment = new AddContactDialogFragment();
        sampleFragment.setArguments(bundle);
        return sampleFragment;
    }

    @SuppressLint("InflateParams")
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
             View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.add_contact, null);
            Toolbar toolbar = (Toolbar) dialogView.findViewById(R.id.toolbar);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toolbar.setTitle("Add Contact");
            }
           utils = new Utils(getActivity());

            etName = (EditText) dialogView.findViewById(R.id.editTextName);
            etName.requestFocus();
            etPhone = (EditText) dialogView.findViewById(R.id.editTextPhone);
            checkFav = (CheckBox)dialogView.findViewById(R.id.checkBoxFav);

             getContactsTast(phoneNumber);

            etName.addTextChangedListener(this);

            if(checkFav.isChecked())
            {
                favourite = "Yes";
            }else
            {
                favourite = "No";
            }

            final AlertDialog alertDialog = new AlertDialog.Builder(getActivity(), R.style.MyWhiteAlertDialogStyle)
                    .setView(dialogView)
                    .setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setPositiveButton("Save", null)
                    .create();
            alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(final DialogInterface dialog) {
                    Button b = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                    b.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            attemptRegistration();


                        }
                    });
                }
            });
            return alertDialog;
        } else {
            return super.onCreateDialog(savedInstanceState);
        }
    }

    private void attemptRegistration() {

        name = etName.getText().toString();

        phone = etPhone.getText().toString();

        boolean cancel = false;
        View focusView = null;


        if (TextUtils.isEmpty(name)) {
            etName.setError("Error - name can't be empty");
            focusView = etName;
            cancel = true;

        }

        if (TextUtils.isEmpty(phone)) {
            etPhone.setError("Error - phone number can't be empty");
            focusView = etPhone;
            cancel = true;

        }

        if (!TextUtils.isEmpty(phone)) {
            if (!utils.validatePhone(phone)) {
                etPhone.setError("Error - Enter correct cellphone");
                focusView = etPhone;
                cancel = true;

            }
        }

        for(MyContact contact : selectedContact)
        {
            if(contact.getName().equals(name))
            {
                etName.setError("Hint - To add second number, go to contacts");
                focusView = etName;
                cancel = true;
            }
        }


        if(checkFav.isChecked())
        {
            favourite = "Yes";
        }else
        {
            favourite = "No";
        }


        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            if (!utils.isOnline(getActivity())) {
                utils.toast(getString(R.string.no_internet_connection));
            } else {

                AddContactTask();



            }
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

        String name = etName.getText().toString();
        for(MyContact contact : selectedContact)
        {
            Log.w("tat ", contact.getName());
            if(name.equals(contact.getName()))
            {
                Log.w("tat ","yes yes yes "+ contact.getName());
                etName.setError("Contact already exist");
                etPhone.setText(contact.getPhone());

            }
        }
    }

    /**
     * Represents an asynchronous AddContact task used to authenticate
     * the user.
     */
    public void AddContactTask() {

        final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setMessage(getString(R.string.add_contacts));
        pd.show();

        final StringRequest request = new StringRequest(Request.Method.POST, Constants.ADD_CONTACT_URL, new Response.Listener<String>(){
            @Override
            public void onResponse(String s) {
                Log.w("TAG", " s "+ s);
                if (s != null){
                    Log.i("Website Content", s);
                    try {
                        JSONObject jsonPart = new JSONObject(s);
                        int num = jsonPart.getInt(TAG_SUCCESS);
                        String message = jsonPart.getString(TAG_MESSAGE);

                        if (num == 1) {

                            Intent intent = new Intent(getActivity(),MainActivity.class);
                            startActivity(intent);


                        } else {

                            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                pd.dismiss();
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                System.out.println("Erroe" + volleyError);
                utils.toast(volleyError.getMessage());
                pd.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> jsonObject = new HashMap<>();
                jsonObject.put("name", name);
                jsonObject.put("phone", phone);
                jsonObject.put("phonetwo", "");
                jsonObject.put("sub_phone", phoneNumber);
                jsonObject.put("fav", favourite);
                jsonObject.put("block", "No");

                Log.w("TAG", " jsonObject "+ jsonObject);
                return jsonObject;
            }


        };

        RequestQueue rQueue = Volley.newRequestQueue(getActivity());
        rQueue.add(request);

    }

    /**
     * Represents an asynchronous getContact task used to authenticate
     * the user.
     */
    public void getContactsTast(final String phone) {

        final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setMessage(getString(R.string.register));
        pd.show();

        final StringRequest request = new StringRequest(Request.Method.POST, Constants.GET_CONTACT_URL, new Response.Listener<String>(){
            @Override
            public void onResponse(String s) {
                Log.w("TAG", " s "+ s);
                if (s != null){
                    Log.i("Website Content", s);
                    try {
                        JSONObject jsonPart = new JSONObject(s);
                        int num = jsonPart.getInt(TAG_SUCCESS);
                        JSONArray jsonArray = jsonPart.getJSONArray(TAG_USER);

                        if (num == 1) {

                            for(int x =0 ; x < jsonArray.length(); x++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(x);

                                //setAddresList(address);
                                String name = jsonObject.getString("name");
                                String phone = jsonObject.getString("phone");
                                String phoneTwo = jsonObject.getString("phonetwo");
                                String fav = jsonObject.getString("fav");
                                String block = jsonObject.getString("block");
                                MyContact myContact = new MyContact(name,phone,phoneTwo,fav,block);
                                if(myContact.getBlocked().equals("No")) {
                                    if (!selectedContact.contains(myContact)) {
                                        selectedContact.add(myContact);
                                    }
                                }

                            }


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                pd.dismiss();
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                System.out.println("Erroe" + volleyError);
                utils.toast(volleyError.getMessage());
                pd.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> jsonObject = new HashMap<>();

                jsonObject.put("phone", phone);

                Log.w("TAG", " jsonObject "+ jsonObject);
                return jsonObject;
            }


        };

        RequestQueue rQueue = Volley.newRequestQueue(getActivity());
        rQueue.add(request);

    }

}
