package tshabalala.bongani.smartcaller.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import tshabalala.bongani.smartcaller.R;
import tshabalala.bongani.smartcaller.activity.MainActivity;
import tshabalala.bongani.smartcaller.helper.Constants;
import tshabalala.bongani.smartcaller.helper.Utils;

/**
 * Created by Bongani on 2017/09/27.
 */


public class BlockContactDialogFragment extends DialogFragment {

    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;
    private EditText etPhone;
    private String phone;

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    Utils utils;


    public static BlockContactDialogFragment newInstance() {

        Bundle bundle = new Bundle();
        BlockContactDialogFragment sampleFragment = new BlockContactDialogFragment();
        sampleFragment.setArguments(bundle);
        return sampleFragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.block_contact, null);
            Toolbar toolbar = (Toolbar) dialogView.findViewById(R.id.toolbar);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toolbar.setTitle("Block Contact");
            }

            utils = new Utils(getActivity());
            etPhone = (EditText) dialogView.findViewById(R.id.editTextPhone);


            final AlertDialog alertDialog = new AlertDialog.Builder(getActivity(), R.style.MyWhiteAlertDialogStyle)
                    .setView(dialogView)
                    .setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setPositiveButton("Block", null)
                    .create();
            alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(final DialogInterface dialog) {
                    Button b = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                    b.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            attemptRegistration();


                        }
                    });
                }
            });
            return alertDialog;
        } else {
            return super.onCreateDialog(savedInstanceState);
        }
    }

    private void attemptRegistration() {

        phone = etPhone.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(phone)) {
            etPhone.setError("Error - phone number can't be empty");
            focusView = etPhone;
            cancel = true;

        }

        if (!TextUtils.isEmpty(phone)) {
            if (!utils.validatePhone(phone)) {
                etPhone.setError("Error - Enter correct cellphone");
                focusView = etPhone;
                cancel = true;

            }
        }



        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            if (!utils.isOnline(getActivity())) {
                Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_LONG).show();
            } else {

                BlockContactTask();

            }
        }
    }

    /**
     * Represents an asynchronous BlockContact task used to authenticate
     * the user.
     */
    public void BlockContactTask() {

        final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setMessage(getString(R.string.block_number));
        pd.show();

        final StringRequest request = new StringRequest(Request.Method.POST, Constants.BLOCK_CONTACT_URL, new Response.Listener<String>(){
            @Override
            public void onResponse(String success) {
                if (success != null){
                    Log.i("Website Content", success);
                    try {
                        JSONObject jsonPart = new JSONObject(success);
                        int num = jsonPart.getInt(TAG_SUCCESS);
                        String message = jsonPart.getString(TAG_MESSAGE);

                        if (num == 1) {

                            Intent intent = new Intent(getActivity(),MainActivity.class);
                            startActivity(intent);
                            Toast.makeText(getActivity(), "Phone number blocked", Toast.LENGTH_LONG).show();

                        } else {
                            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                pd.dismiss();
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                System.out.println("Erroe" + volleyError);
                Toast.makeText(getActivity(), volleyError.getMessage(), Toast.LENGTH_LONG).show();
                pd.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> jsonObject = new HashMap<>();
                jsonObject.put("block", "Yes");
                jsonObject.put("phone", phone);
                return jsonObject;
            }


        };

        RequestQueue rQueue = Volley.newRequestQueue(getActivity());
        rQueue.add(request);

    }



}
