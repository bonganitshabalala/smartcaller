package tshabalala.bongani.smartcaller.fragment;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tshabalala.bongani.smartcaller.R;
import tshabalala.bongani.smartcaller.activity.MainActivity;
import tshabalala.bongani.smartcaller.adapter.BlockedAdapter;
import tshabalala.bongani.smartcaller.helper.Constants;
import tshabalala.bongani.smartcaller.helper.MyContact;

/**
 * Created by Bongani on 2017/09/13.
 */

public class BlockFragment  extends Fragment {

    FragmentManager fm;

    List<MyContact> selectedContact = new ArrayList<>();
    ListView listView;

    BlockedAdapter arrayAdapter;
    String strPhone;

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    private static final String TAG_USER = "con";
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;

    TelephonyManager telephonyManager;
    BroadcastReceiver br;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.tab_block_main, container, false);

        fm = getActivity().getSupportFragmentManager();

        SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        String  phoneNumber = mSharedPreferences.getString("phone", "");

        listView = (ListView)rootView.findViewById(R.id.list_item);

        getContactsTast(phoneNumber);

        telephonyManager = ((TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE));
        MyPhoneListener phoneListener = new MyPhoneListener();
        telephonyManager.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);

        arrayAdapter = new BlockedAdapter(getActivity(),android.R.string.selectTextMode,selectedContact);
        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                strPhone = selectedContact.get(i).getPhone();


                BlockContactTast(strPhone);


            }
        });
        IntentFilter filter = new IntentFilter("android.intent.action.PHONE_STATE");
        getActivity().registerReceiver(br, filter);

        FloatingActionButton btnAdd = (FloatingActionButton)rootView.findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                BlockContactDialogFragment newEventDevice = BlockContactDialogFragment.newInstance();
                android.support.v4.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
                Fragment previous = getFragmentManager().findFragmentByTag("newEventDialog");
                if (previous != null){
                    transaction.remove(previous);
                }
                transaction.add(newEventDevice, "newEventDialog");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        return rootView;
    }

    /**
     * Represents an asynchronous getContact task used to authenticate
     * the user.
     */
    public void getContactsTast(final String phone) {

        final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setMessage(getString(R.string.register));
        pd.show();

        final StringRequest request = new StringRequest(Request.Method.POST, Constants.GET_CONTACT_URL, new Response.Listener<String>(){
            @Override
            public void onResponse(String success) {
                if (success != null){
                    Log.i("Website Content", success);
                    try {
                        JSONObject jsonPart = new JSONObject(success);
                        int num = jsonPart.getInt(TAG_SUCCESS);
                        JSONArray jsonArray = jsonPart.getJSONArray(TAG_USER);

                        if (num == 1) {

                            for(int x =0 ; x < jsonArray.length(); x++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(x);

                                String name = jsonObject.getString("name");
                                String phone = jsonObject.getString("phone");
                                String phonetwo = jsonObject.getString("phonetwo");
                                String fav = jsonObject.getString("fav");
                                String block = jsonObject.getString("block");
                                MyContact myContact = new MyContact(name,phone,phonetwo,fav,block);
                                if(myContact.getBlocked().equals("Yes")) {
                                    if (!selectedContact.contains(myContact)) {
                                        selectedContact.add(myContact);
                                    }
                                }

                                arrayAdapter = new BlockedAdapter(getActivity(),android.R.string.selectTextMode,selectedContact);
                                listView.setAdapter(arrayAdapter);
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                pd.dismiss();
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                System.out.println("Erroe" + volleyError);
                Toast.makeText(getActivity(), volleyError.getMessage(), Toast.LENGTH_LONG).show();
                pd.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> jsonObject = new HashMap<>();

                jsonObject.put("phone", phone);

                Log.w("TAG", " jsonObject "+ jsonObject);
                return jsonObject;
            }


        };

        RequestQueue rQueue = Volley.newRequestQueue(getActivity());
        rQueue.add(request);

    }

    /**
     * Represents an asynchronous UnBlockContact task used to authenticate
     * the user.
     */
    public void BlockContactTast(final String phone) {

        final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setMessage(getString(R.string.unblock_number));
        pd.show();

        final StringRequest request = new StringRequest(Request.Method.POST, Constants.BLOCK_CONTACT_URL, new Response.Listener<String>(){
            @Override
            public void onResponse(String success) {
                if (success != null){
                    try {
                        JSONObject jsonPart = new JSONObject(success);
                        int num = jsonPart.getInt(TAG_SUCCESS);
                        String message = jsonPart.getString(TAG_MESSAGE);

                        if (num == 1) {

                            Gson gson = new Gson();
                            SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
                            String json = gson.toJson(selectedContact);
                            SharedPreferences.Editor editor = mPrefs.edit();
                            editor.putString("contact", json).apply();

                            Intent intent = new Intent(getActivity(),MainActivity.class);
                            startActivity(intent);
                            Toast.makeText(getActivity(), "Phone number unblocked", Toast.LENGTH_LONG).show();


                        } else {
                            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                pd.dismiss();
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                System.out.println("Erroe" + volleyError);
                Toast.makeText(getActivity(), volleyError.getMessage(), Toast.LENGTH_LONG).show();
                pd.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> jsonObject = new HashMap<>();
                jsonObject.put("block", "No");
                jsonObject.put("phone", phone);
                return jsonObject;
            }


        };

        RequestQueue rQueue = Volley.newRequestQueue(getActivity());
        rQueue.add(request);

    }

    private class MyPhoneListener extends PhoneStateListener {

        private boolean onCall = false;

        @Override

        public void onCallStateChanged(int state, String incomingNumber) {

            switch (state) {


                case TelephonyManager.CALL_STATE_RINGING:
                    // phone ringing...

                        Log.w("TAG","WHERE AR YOU");
                    break;



                case TelephonyManager.CALL_STATE_OFFHOOK:
                    Log.w("TAG","DONE");

                    onCall = true;

                    break;



                case TelephonyManager.CALL_STATE_IDLE:

                    // in initialization of the class and at the end of phone call
                    // detect flag from CALL_STATE_OFFHOOK

                    if (onCall) {

                        onCall = false;

                    }

                    break;

                default:

                    break;

            }



        }

    }

}
