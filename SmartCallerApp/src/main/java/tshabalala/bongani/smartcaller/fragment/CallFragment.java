package tshabalala.bongani.smartcaller.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tshabalala.bongani.smartcaller.R;
import tshabalala.bongani.smartcaller.activity.ViewContactActivity;
import tshabalala.bongani.smartcaller.adapter.ListViewAdapter;
import tshabalala.bongani.smartcaller.helper.Constants;
import tshabalala.bongani.smartcaller.helper.MyContact;

/**
 * Created by Bongani on 2017/09/13.
 */

public class CallFragment extends Fragment implements SearchView.OnQueryTextListener{

    String phoneNumber;
    List<MyContact> selectedContact = new ArrayList<>();

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_USER = "con";
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;

    TelephonyManager telephonyManager;
    // Declare Variables
    ListView list;
    ListViewAdapter adapter;
    SearchView editsearch;


    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);

    }


        @Override
        public View onCreateView(LayoutInflater inflater,@Nullable ViewGroup container,@Nullable Bundle savedInstanceState) {


            View rootView = inflater.inflate(R.layout.search_act, container, false);
            // Generate sample data
            SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            phoneNumber = mSharedPreferences.getString("phone", "");


            list = (ListView) rootView.findViewById(R.id.listview);
            getContactsTast(phoneNumber);

            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    Intent intent = new Intent(getActivity(), ViewContactActivity.class);
                    intent.putExtra("contact",selectedContact.get(i));
                    intent.putExtra("sub",phoneNumber);
                    getActivity().startActivity(intent);
                }
            });

            telephonyManager = ((TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE));

            editsearch = (SearchView) rootView.findViewById(R.id.search);
            editsearch.setOnQueryTextListener(this);

            return rootView;
        }

    @Override
    public boolean onQueryTextSubmit(String query) {

        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        try {
            adapter.filter(newText);
        }catch (Exception x)
        {
            x.getMessage();
        }
        return false;
    }

    /**
     * Represents an asynchronous getContact task used to authenticate
     * the user.
     */
    public void getContactsTast(final String phone) {

        final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setMessage(getString(R.string.register));
        pd.show();

        final StringRequest request = new StringRequest(Request.Method.POST, Constants.GET_CONTACT_URL, new Response.Listener<String>(){
            @Override
            public void onResponse(String success) {
                if (success != null){
                    Log.i("Website Content", success);
                    try {
                        JSONObject jsonPart = new JSONObject(success);
                        int num = jsonPart.getInt(TAG_SUCCESS);
                        JSONArray jsonArray = jsonPart.getJSONArray(TAG_USER);

                        if (num == 1) {

                            for(int x =0 ; x < jsonArray.length(); x++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(x);

                                String name = jsonObject.getString("name");
                                String phone = jsonObject.getString("phone");
                                String phoneTwo = jsonObject.getString("phonetwo");
                                String fav = jsonObject.getString("fav");
                                String block = jsonObject.getString("block");
                                MyContact myContact = new MyContact(name,phone,phoneTwo,fav,block);
                                Gson gson = new Gson();
                                SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
                                if(myContact.getBlocked().equals("No")) {
                                    if (!selectedContact.contains(myContact)) {
                                        selectedContact.add(myContact);
                                    }
                                    String json = gson.toJson(selectedContact);
                                    SharedPreferences.Editor editor = mPrefs.edit();
                                    editor.putString("contact", json).apply();

                                }else
                                {

                                    if (!selectedContact.contains(myContact)) {
                                        selectedContact.add(myContact);
                                    }


                                    String json = gson.toJson(selectedContact);
                                    SharedPreferences.Editor editor = mPrefs.edit();
                                    editor.putString("contact", json).apply();

                                }

                                adapter = new ListViewAdapter(getActivity(), selectedContact);

                                // Binds the Adapter to the ListView
                                list.setAdapter(adapter);


                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                pd.dismiss();
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                System.out.println("Erroe" + volleyError);
                Toast.makeText(getActivity(),volleyError.getMessage(),Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> jsonObject = new HashMap<>();

                jsonObject.put("phone", phone);

                Log.w("TAG", " jsonObject "+ jsonObject);
                return jsonObject;
            }


        };

        RequestQueue rQueue = Volley.newRequestQueue(getActivity());
        rQueue.add(request);

    }


}
