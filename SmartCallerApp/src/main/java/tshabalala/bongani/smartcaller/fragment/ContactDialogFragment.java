package tshabalala.bongani.smartcaller.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.CallLog;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import tshabalala.bongani.smartcaller.R;
import tshabalala.bongani.smartcaller.activity.MainActivity;
import tshabalala.bongani.smartcaller.helper.Constants;

/**
 * Created by Bongani on 2017/09/23.
 */

public class ContactDialogFragment extends DialogFragment implements NumberPicker.OnValueChangeListener {


    static String phone = null;
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";

    private int min;
    TelephonyManager telephonyManager;

    NumberPicker np;
    static String names, subscriberphone;
    String duration;
    String time;

    public static ContactDialogFragment newInstance(String con, String name, String subscriber) {
        Bundle args = new Bundle();
        subscriberphone = subscriber;
        names = name;
        phone = con;
        ContactDialogFragment frag = new ContactDialogFragment();
        frag.setArguments(args);
        return frag;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.tab_call_main, null);
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setTitle("Call Contact");
        }
        EditText etnumber = (EditText) view.findViewById(R.id.editTextNumber);
        etnumber.setText(phone);

        np = (NumberPicker) view.findViewById(R.id.np);
        np.setMinValue(0);
        //Specify the maximum value/number of NumberPicker
        np.setMaxValue(59);

        //Gets whether the selector wheel wraps when reaching the min/max value.
        np.setWrapSelectorWheel(true);

        //Set a value change listener for NumberPicker
        np.setOnValueChangedListener(this);
        Button btnCall = (Button) view.findViewById(R.id.buttonCall);
        telephonyManager = ((TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE));

        // receive notifications of telephony state changes
        MyPhoneListener phoneListener = new MyPhoneListener();
        telephonyManager.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (min > 0) {

                    TelephonyManager  telephonyManager = ((TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE));
                    assert telephonyManager != null;
                    String  carrierName = telephonyManager.getNetworkOperatorName().trim();
                    Log.i("TAG"," operator "+ carrierName);
                    String callRate = "";

                    switch (carrierName) {
                        case "Cell C":

                            callRate = "Your on R0.99 per minute";
                            break;
                        case "Vodacom":
                            callRate = "Your on R0.79 per minute";
                            break;
                        case "MTN":
                            callRate = "Your on R1.50 per minute";
                            break;
                    }

                    Toast.makeText(getActivity(),callRate,Toast.LENGTH_LONG).show();
                    System.out.print("Hell " + min);
                    String uri = "tel:" + phone;
                    Uri call = Uri.parse(uri);
                    DateFormat dateFormat1 = new SimpleDateFormat("HH:mm", Locale.getDefault());
                    Calendar cal = Calendar.getInstance();
                    time = dateFormat1.format(cal.getTime());
                    Intent surf = new Intent(Intent.ACTION_CALL, call);
                    getActivity().startActivity(surf);

                } else {
                    Toast.makeText(getActivity(), "Minute must be greater than 0", Toast.LENGTH_LONG).show();
                }


            }
        });


        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity(), R.style.MyWhiteAlertDialogStyle)
                .setView(view)
                .setPositiveButton("Cancel", null)
                .create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button btnAccept = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                btnAccept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        alertDialog.dismiss();

                    }
                });
            }
        });

        return alertDialog;
    }

    @Override
    public void onStart() {
        super.onStart();

        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void onValueChange(NumberPicker numberPicker, int i, int i1) {

        min = i1;

    }

    private class MyPhoneListener extends PhoneStateListener {

        private boolean onCall = false;

        DateFormat dateFormat = new SimpleDateFormat("dd/MMMM/yyyy", Locale.getDefault());
        Date date = new Date();
        String dateString = dateFormat.format(date);

        @Override

        public void onCallStateChanged(int state, String incomingNumber) {


            switch (state) {


                case TelephonyManager.CALL_STATE_RINGING:
                    // phone ringing...
                    Log.w("TAG", "dura ringing "+ duration);
                    onCall = true;


                    break;


                case TelephonyManager.CALL_STATE_OFFHOOK:

                    // one call exists that is dialing, active, or on hold
                    //Toast.makeText(getActivity()," call exists that is dialing",Toast.LENGTH_LONG).show();
                    try {

                        int secondsDelayed = min * 60;
                        System.out.print("secondsDelayed " + secondsDelayed + " min " + min);
                        new Handler().postDelayed(new Runnable() {
                            public void run() {

                                //getActivity().finish();

                                try {
                                    Class classs = Class.forName(telephonyManager.getClass().getName());
                                    Method method = classs.getDeclaredMethod("getITelephony");
                                    method.setAccessible(true);
                                    Object teleService = method.invoke(telephonyManager);
                                    classs = Class.forName(teleService.getClass().getName());
                                    method = classs.getDeclaredMethod("endCall");
                                    method.setAccessible(true);
                                    method.invoke(teleService);


                                } catch (ClassNotFoundException e) {
                                    e.printStackTrace();
                                } catch (NoSuchMethodException e) {
                                    e.printStackTrace();
                                } catch (InvocationTargetException e) {
                                    e.printStackTrace();
                                } catch (IllegalAccessException e) {
                                    e.printStackTrace();
                                }


                            }
                        }, secondsDelayed * 1000);


                    } catch (Exception z) {
                        z.getMessage();
                    }

                    onCall = true;

                    break;


                case TelephonyManager.CALL_STATE_IDLE:

                    if (onCall) {

                        final ContentResolver resolver = getActivity().getContentResolver();

                        int  sec = 5000;

                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                Cursor curs = resolver.query(CallLog.Calls.CONTENT_URI, null, null, null, android.provider.CallLog.Calls.DATE + " DESC limit 1;");
                                while (curs.moveToNext()) {

                                    duration = curs.getString(curs
                                            .getColumnIndex(android.provider.CallLog.Calls.DURATION));


                                }
                            }
                        }, sec * 1000);

                        CallHistoryTask(names, phone, dateString, time, duration);

                        onCall = false;

                    }

                    break;

                default:

                    break;

            }




        }

    }

    private void CallHistoryTask(final String name,final String phone,final String dateString,final String time,final String duration) {

        final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setMessage(getString(R.string.add_contacts));
        pd.show();

        final StringRequest request = new StringRequest(Request.Method.POST, Constants.STORE_CALL_HISTORY_URL, new Response.Listener<String>(){
            @Override
            public void onResponse(String success) {
                if (success != null){
                    Log.i("Website Content", success);
                    try {
                        JSONObject jsonPart = new JSONObject(success);
                        int num = jsonPart.getInt(TAG_SUCCESS);
                        String message = jsonPart.getString(TAG_MESSAGE);

                        if (num == 1) {

                            Intent intent = new Intent(getActivity(), MainActivity.class);
                            startActivity(intent);

                        } else {

                            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                pd.dismiss();
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                System.out.println("Erroe" + volleyError);
                Toast.makeText(getActivity(),volleyError.getMessage(), Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> jsonObject = new HashMap<>();
                jsonObject.put("name", name);
                jsonObject.put("phone", phone);
                jsonObject.put("date", dateString);
                jsonObject.put("time", time);
                jsonObject.put("duration", duration);
                jsonObject.put("sub", subscriberphone);

                Log.w("TAG", " jsonObject "+ jsonObject);
                return jsonObject;
            }


        };

        RequestQueue rQueue = Volley.newRequestQueue(getActivity());
        rQueue.add(request);

    }



}