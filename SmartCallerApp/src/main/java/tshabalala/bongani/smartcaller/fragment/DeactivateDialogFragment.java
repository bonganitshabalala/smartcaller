package tshabalala.bongani.smartcaller.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import tshabalala.bongani.smartcaller.R;
import tshabalala.bongani.smartcaller.activity.HomeActivity;
import tshabalala.bongani.smartcaller.helper.Constants;
import tshabalala.bongani.smartcaller.helper.Utils;


/**
 * Created by Bongani on 2017/09/04.
 */

public class DeactivateDialogFragment extends DialogFragment {

    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;
    private static String email;

    private static final String TAG_SUCCESS = "success";

    Utils utils;


    public static DeactivateDialogFragment newInstance(String em) {

        Bundle bundle = new Bundle();
        email = em;
        DeactivateDialogFragment sampleFragment =  new DeactivateDialogFragment();
        sampleFragment.setArguments(bundle);
        return sampleFragment;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (savedInstanceState == null){
            View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_deactivate, null);

            utils = new Utils(getActivity());
            Log.w("TAg"," DeactivateDialogFragment "+ email);
            final AlertDialog alertDialog = new AlertDialog.Builder(getActivity(), R.style.MyWhiteAlertDialogStyle)
                    .setView(dialogView)
                    .setTitle("Deactivate account")
                    .setMessage("Are you sure you want to deactivate account?")
                    .setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setPositiveButton("Confirm", null)
                    .create();
            alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(final DialogInterface dialog) {
                    Button b = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                    b.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            DeleteSubscriberTast(email);


                        }
                    });
                }
            });
            return alertDialog;
        } else {
            return super.onCreateDialog(savedInstanceState);
        }
    }

    /**
     * Represents an asynchronous DeleteSubscriber task used to authenticate
     * the user.
     */
    public void DeleteSubscriberTast(final String email) {

        final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setMessage(getString(R.string.delete_account));
        pd.show();

        final StringRequest request = new StringRequest(Request.Method.POST, Constants.DELETE_SUBSCRIBER_URL, new Response.Listener<String>(){
            @Override
            public void onResponse(String success) {

                if (success != null){
                    Log.i("Hello",success);
                    try {

                        JSONObject jsonPart = new JSONObject(success);
                        int num = jsonPart.getInt(TAG_SUCCESS);
                        String message = jsonPart.getString(TAG_SUCCESS);

                        if (num == 1) {

                            SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
                            SharedPreferences.Editor editor = mPrefs.edit();
                            editor.clear().apply();

                            startActivity(new Intent(getActivity(),HomeActivity.class));
                            getActivity().finish();

                        } else {
                            utils.toast(message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                pd.dismiss();
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                System.out.println("Erroe" + volleyError);
                utils.toast(volleyError.getMessage());
                pd.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> jsonObject = new HashMap<>();

                jsonObject.put("phone", email);

                return jsonObject;
            }


        };

        RequestQueue rQueue = Volley.newRequestQueue(getActivity());
        rQueue.add(request);

    }

}
