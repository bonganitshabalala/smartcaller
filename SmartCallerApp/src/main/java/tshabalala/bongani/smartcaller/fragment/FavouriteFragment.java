package tshabalala.bongani.smartcaller.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.opencsv.CSVWriter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import tshabalala.bongani.smartcaller.R;
import tshabalala.bongani.smartcaller.adapter.FavouriteAdapter;
import tshabalala.bongani.smartcaller.adapter.LogAdapter;
import tshabalala.bongani.smartcaller.helper.Constants;
import tshabalala.bongani.smartcaller.helper.MyContact;
import tshabalala.bongani.smartcaller.helper.Utils;


/**
 * Created by Bongani on 2017/09/13.
 */

public class FavouriteFragment  extends Fragment {

    FragmentManager fm;
    ListView listView;

    FavouriteAdapter arrayAdapter;

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_USER = "con";
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;

    List<MyContact> selectedContact = new ArrayList<>();

    private static final String logDir = "/LogData";
    private static final String logFile = "/FavouriteContact";


    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);

    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.tab_fav_main, container, false);

        fm = getActivity().getSupportFragmentManager();

        listView = (ListView)rootView.findViewById(R.id.list_item);

        SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

       String phoneNumber = mSharedPreferences.getString("phone", "");

        getContactsTast(phoneNumber);

        Button btnReport = (Button) rootView.findViewById(R.id.buttonReport);
        btnReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeLog();
            }
        });

        List<MyContact> contacts = new ArrayList<>();

        for(MyContact myContact : selectedContact) {
            if(myContact.getFavourite().equals("Yes")) {

                contacts.add(myContact);
            }
        }


            arrayAdapter = new FavouriteAdapter(getActivity(), android.R.string.selectTextMode, contacts);
            listView.setAdapter(arrayAdapter);



        return rootView;
    }

    public void makeLog() {
        try {

            final ListView listView = new ListView(getActivity());
            listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);

            final LogAdapter logAdapter = new LogAdapter(getActivity(), R.id.textView, selectedContact);
            listView.setAdapter(logAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    logAdapter.notifyDataSetChanged();
                }
            });

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.MyWhiteAlertDialogStyle);
            builder.setTitle("Activities Log - ");
            builder.setView(listView);
            builder.setNeutralButton("Store Favourites", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //Write Raw Frame Data to File

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                boolean success;
                                File dir = new File(Environment.getExternalStorageDirectory() + logDir);

                                success = (dir.mkdir() || dir.isDirectory());
                                if (success) {
                                    String timelabel = "_" + Utils.getDateTime(new Date()).replace(" ", "_").replace(":", "h").replace("/", "-");
                                    CSVWriter writer = new CSVWriter(new FileWriter(dir + logFile + timelabel + ".csv"), ',');
                                    for (ListIterator<MyContact> i = selectedContact.listIterator(); i.hasNext(); ) {
                                        MyContact entry = i.next();
                                        if (entry.getFavourite().equals("Yes")) {
                                            String[] entries = (entry.getName() + " : " + entry.getPhone()).split(":");
                                            writer.writeNext(entries);
                                        }
                                    }

                                    writer.close();


//                                            }
                                } else {
                                    Toast.makeText(getActivity(), "Could not create directory \"/LogData\"", Toast.LENGTH_SHORT).show();
                                }

                                //printLog("Log written to file...");
                            } catch (Exception e) {
                                e.getStackTrace();
                            }
                        }
                    }, "writeFileThread").start();
                    Toast.makeText(getActivity(), "Successfully created file", Toast.LENGTH_SHORT).show();

                }
            });
            builder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (dialog != null)
                        dialog.dismiss();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
            listView.smoothScrollToPosition(logAdapter.getCount()-1);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        } catch (Exception e){
            e.getStackTrace();
        }
    }

    /**
     * Represents an asynchronous getContact task used to authenticate
     * the user.
     */
    public void getContactsTast(final String phone) {

        final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setMessage(getString(R.string.register));
        pd.show();

        final StringRequest request = new StringRequest(Request.Method.POST, Constants.GET_CONTACT_URL, new Response.Listener<String>(){
            @Override
            public void onResponse(String success) {
                if (success != null){
                    Log.i("Website Content", success);
                    try {
                        JSONObject jsonPart = new JSONObject(success);
                        int num = jsonPart.getInt(TAG_SUCCESS);
                        JSONArray jsonArray = jsonPart.getJSONArray(TAG_USER);
                        MyContact myContact;
                        if (num == 1) {

                            for(int x =0 ; x < jsonArray.length(); x++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(x);

                                String name = jsonObject.getString("name");
                                String phone = jsonObject.getString("phone");
                                String phonetwo = jsonObject.getString("phonetwo");
                                String fav = jsonObject.getString("fav");
                                String block = jsonObject.getString("block");
                                myContact = new MyContact(name,phone,phonetwo,fav,block);

                                    if(myContact.getBlocked().equals("No")) {
                                        if (!selectedContact.contains(myContact)) {
                                            selectedContact.add(myContact);
                                        }
                                    }


                            }

                            List<MyContact> contacts = new ArrayList<>();

                            for(MyContact myContac : selectedContact) {
                                if(myContac.getFavourite().equals("Yes")) {

                                    contacts.add(myContac);
                                }
                            }
                            arrayAdapter = new FavouriteAdapter(getActivity(), android.R.string.selectTextMode, contacts);
                            listView.setAdapter(arrayAdapter);


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                pd.dismiss();
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                System.out.println("Erroe" + volleyError);
                Toast.makeText(getActivity(), volleyError.getMessage(), Toast.LENGTH_LONG).show();
                pd.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> jsonObject = new HashMap<>();

                jsonObject.put("phone", phone);

                Log.w("TAG", " jsonObject "+ jsonObject);
                return jsonObject;
            }


        };

        RequestQueue rQueue = Volley.newRequestQueue(getActivity());
        rQueue.add(request);

    }


}
