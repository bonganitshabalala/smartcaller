package tshabalala.bongani.smartcaller.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.regex.Pattern;

import tshabalala.bongani.smartcaller.R;
import tshabalala.bongani.smartcaller.activity.MainActivity;

/**
 * Created by Bongani on 2017/08/24.
 */

public class ForgotDialogFragment extends DialogFragment
{
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;
    private static String urlConne = "http://frothiest-wrenches.000webhostapp.com/retrievePassword.php";
    private String phone;
    private EditText etNumber;
    Activity activity;
    private ProgressDialog pDialog;

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";


    public static ForgotDialogFragment newInstance() {

        Bundle bundle = new Bundle();
        ForgotDialogFragment sampleFragment =  new ForgotDialogFragment();
        sampleFragment.setArguments(bundle);
        return sampleFragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (savedInstanceState == null){
            View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_add_contact, null);

            etNumber = (EditText) dialogView.findViewById(R.id.editTextEmail);

            final AlertDialog alertDialog = new AlertDialog.Builder(getActivity(), R.style.MyWhiteAlertDialogStyle)
                    .setView(dialogView)
                    .setTitle("Reset password")
                    .setMessage("Enter your email to reset password")
                    .setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setPositiveButton("Confirm", null)
                    .create();
            alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(final DialogInterface dialog) {
                    Button b = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                    b.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            attemptRegistration();

                        }
                    });
                }
            });
            return alertDialog;
        } else {
            return super.onCreateDialog(savedInstanceState);
        }
    }

    private void attemptRegistration() {

        phone = etNumber.getText().toString();


        boolean cancel = false;
        View focusView = null;


        if (TextUtils.isEmpty(phone)) {
            etNumber.setError("Error - email can't be empty");
            focusView = etNumber;
            cancel = true;

        }

        if (!isEmailValid(phone)) {
            etNumber.setError("Error - Enter correct email");
            focusView = etNumber;
            cancel = true;

        }



        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            if (!isOnline(activity)) {
                Toast.makeText(activity, "No internet connection", Toast.LENGTH_LONG).show();
            } else {

                new sendInfor(phone).execute();

            }
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        boolean valid;
        String emailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        if (Pattern.matches(emailPattern, email))
        {

            valid = true;


        }else {
            valid = false;
        }

        return valid;
    }

    private boolean isOnline(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    public void sendMySMS(String phone,String message) {


        SmsManager sms = SmsManager.getDefault();
        // if message length is too long messages are divided
        List<String> messages = sms.divideMessage(message);
        for (String msg : messages) {

            PendingIntent sentIntent = PendingIntent.getBroadcast(activity, 0, new Intent("SMS_SENT"), 0);
            PendingIntent deliveredIntent = PendingIntent.getBroadcast(activity, 0, new Intent("SMS_DELIVERED"), 0);
            sms.sendTextMessage(phone, null, msg, sentIntent, deliveredIntent);

        }

    }

    public class sendInfor extends AsyncTask<String, String, String> {

        HttpURLConnection conn;
        URL url = null;
        String code = "";

        public sendInfor(String code) {

            this.code = code;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(activity);
            pDialog.setMessage("Sending request...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO: attempt authentication against a network service.

            try {

                // Enter URL address where your php file resides
                url = new URL(urlConne);

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("phone", code)
                        .appendQueryParameter("status", "Pending");
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                Log.d("response_code", "" + response_code);
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    // Pass data to onPostExecute method
                    return (result.toString());

                } else {

                    return ("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }

        }

        @Override
        protected void onPostExecute(String success) {

            pDialog.dismiss();
            if (success != null) {
                Log.i("Website Content", success);
                try {

                    JSONObject jsonPart = new JSONObject(success);
                    Log.i("Website Content", "" + jsonPart.toString());
                    int num = jsonPart.getInt(TAG_SUCCESS);
                    String msg = jsonPart.getString(TAG_MESSAGE);

                    if (num == 1) {

                        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();

                        String data = "Apollo Locator wants to keep track of your location in case of an emergency. Download it from ";

                        sendMySMS(code,data);      //TODO closure
                        Intent intent = new Intent(activity, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);


                    } else {
                        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}