package tshabalala.bongani.smartcaller.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tshabalala.bongani.smartcaller.R;
import tshabalala.bongani.smartcaller.adapter.HistoryAdapter;
import tshabalala.bongani.smartcaller.helper.Constants;
import tshabalala.bongani.smartcaller.helper.History;


/**
 * Created by Bongani on 2017/09/13.
 */

public class HistoryFragment  extends Fragment {

    FragmentManager fm;


    List<History> histories = new ArrayList<>();

    ListView listView;

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_USER = "history";
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;


    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.tab_history_main, container, false);

        fm = getActivity().getSupportFragmentManager();

        SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String  phoneNumber = mSharedPreferences.getString("phone", "");

        GetCallHistoryTast(phoneNumber);

        listView = (ListView) rootView.findViewById(R.id.list_item);


        HistoryAdapter arrayAdapter = new HistoryAdapter(getActivity(),android.R.string.selectTextMode,histories);
        listView.setAdapter(arrayAdapter);

        return rootView;
    }


    /**
     * Represents an asynchronous getCallLogs task used to authenticate
     * the user.
     */
    public void GetCallHistoryTast(final String phone) {

        final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setMessage(getString(R.string.get_history_log));
        pd.show();

        final StringRequest request = new StringRequest(Request.Method.POST, Constants.GET_HISTORY_URL, new Response.Listener<String>(){
            @Override
            public void onResponse(String success) {
                if (success != null){
                    Log.i("Website Content", success);
                    try {
                        JSONObject jsonPart = new JSONObject(success);
                        int num = jsonPart.getInt(TAG_SUCCESS);
                        JSONArray jsonArray = jsonPart.getJSONArray(TAG_USER);

                        if (num == 1) {

                            for(int x =0 ; x < jsonArray.length(); x++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(x);

                                String name = jsonObject.getString("name");
                                String phone = jsonObject.getString("phone");
                                String date = jsonObject.getString("date");
                                String time = jsonObject.getString("time");
                                String duration = jsonObject.getString("duration");
                                History myContact = new History(name,phone,date,time,duration);

                                if (!histories.contains(myContact)) {
                                    histories.add(myContact);
                                }

                            }

                            HistoryAdapter arrayAdapter = new HistoryAdapter(getActivity(),android.R.string.selectTextMode,histories);
                            listView.setAdapter(arrayAdapter);


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                pd.dismiss();
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                System.out.println("Erroe" + volleyError);
                Toast.makeText(getActivity(),volleyError.getMessage(),Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> jsonObject = new HashMap<>();

                jsonObject.put("subscriberphone", phone);

                return jsonObject;
            }


        };

        RequestQueue rQueue = Volley.newRequestQueue(getActivity());
        rQueue.add(request);

    }


}
