package tshabalala.bongani.smartcaller.helper;

public class Constants {

    public static final String HOME_LOGIN_URL = "http://app-1505651429.000webhostapp.com/smartLogin.php";

    public static final String VERIFICATION_URL = "http://app-1505651429.000webhostapp.com/registerSubscriber.php";

    public static final String MAIN_URL = "http://app-1505651429.000webhostapp.com/getContact.php";

    public static final String ADD_CONTACT_URL = "http://app-1505651429.000webhostapp.com/addContact.php";

    public static final String GET_CONTACT_URL = "http://app-1505651429.000webhostapp.com/getContact.php";

    public static final String GET_SUBSCRIBER_URL = "http://app-1505651429.000webhostapp.com/getSubscriber.php";

    public static final String EDIT_SUBSCRIBER_URL = "http://app-1505651429.000webhostapp.com/editSubscriber.php";

    public static final String DELETE_SUBSCRIBER_URL = "http://app-1505651429.000webhostapp.com/deleteSub.php";

    public static final String GET_HISTORY_URL = "http://app-1505651429.000webhostapp.com/getHistory.php";

    public static final String DELETE_CONTACT_URL = "http://app-1505651429.000webhostapp.com/deleteContact.php";

    public static final String STORE_CALL_HISTORY_URL = "http://app-1505651429.000webhostapp.com/storeHistory.php";

    public static final String UPDATE_CONTACT_URL = "http://app-1505651429.000webhostapp.com/updateContact.php";

    public static final String BLOCK_CONTACT_URL = "http://app-1505651429.000webhostapp.com/blockContact.php";

    public static final String RETRIEVE_PASSWORD_URL = "http://frothiest-wrenches.000webhostapp.com/retrievePassword.php";


}
