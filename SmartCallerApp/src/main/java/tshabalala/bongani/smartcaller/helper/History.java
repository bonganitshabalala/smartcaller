package tshabalala.bongani.smartcaller.helper;

/**
 * Created by Bongani on 2017/09/26.
 */

public class History {

    private String name;
    private String phone;
    private String date;
    private String time;
    private String duration;

    public History() {
    }

    public History(String name, String phone, String date, String duration) {
        this.name = name;
        this.phone = phone;
        this.date = date;
        this.duration = duration;
    }

    public History(String name, String phone, String date,String time, String duration) {
        this.name = name;
        this.phone = phone;
        this.date = date;
        this.time = time;
        this.duration = duration;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
