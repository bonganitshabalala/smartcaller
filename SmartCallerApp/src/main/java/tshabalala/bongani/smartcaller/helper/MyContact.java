package tshabalala.bongani.smartcaller.helper;

import java.io.Serializable;

/**
 * Created by Bongani on 2017/08/29.
 */

public class MyContact implements Serializable{

    public String name;
    public String phone;
    public String phoneTwo;
    public int id;
    public String favourite;
    private String blocked;

    public MyContact() {
    }

    public MyContact(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }

    public MyContact(String name, String phone,String phoneTwo,String favourite,String blocked) {
        this.name = name;
        this.phone = phone;
        this.phoneTwo = phoneTwo;
        this.favourite = favourite;
        this.blocked = blocked;
    }

    public String getPhoneTwo() {
        return phoneTwo;
    }

    public void setPhoneTwo(String phoneTwo) {
        this.phoneTwo = phoneTwo;
    }

    public String getFavourite() {
        return favourite;
    }

    public void setFavourite(String favourite) {
        this.favourite = favourite;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getBlocked() {
        return blocked;
    }

    public void setBlocked(String blocked) {
        this.blocked =  blocked;
    }

}
