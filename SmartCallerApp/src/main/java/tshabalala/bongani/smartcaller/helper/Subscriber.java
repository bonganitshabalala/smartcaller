package tshabalala.bongani.smartcaller.helper;

/**
 * Created by Bongani on 2017/08/24.
 */

public class Subscriber {

    private String name;
    private String surname;
    private String phone;
    private String province;
    private String city;
    private String IDnumber;
    private String Gender;
    private String Age;

    public Subscriber(String name, String surname, String phone,String province,String city,String IDnumber,String Gender,String Age) {

        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.province = province;
        this.city = city;
        this.IDnumber = IDnumber;
        this.Gender = Gender;
        this.Age = Age;
    }

    public Subscriber() {
    }

    public String getIDnumber() {
        return IDnumber;
    }

    public void setIDnumber(String IDnumber) {
        this.IDnumber = IDnumber;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getAge() {
        return Age;
    }

    public void setAge(String age) {
        Age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }


}
