package tshabalala.bongani.smartcaller.helper;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Pattern;

/**
 * Created by Bongani on 2016/11/10.
 */
public class Utils {


    public Utils(Activity activity){
        mActivity = activity;
        mUiThreadId = Thread.currentThread().getId();
    }

    List<File> inFileNames= new ArrayList<File>();

    public static boolean isExternalStorageReadOnly() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    public static boolean isExternalStorageAvailable() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    public static boolean copyFileData(File src,File dst)throws IOException {

        Log.e("LOcation :: ", "source "+ src + " disto " + dst);
        if(src.getAbsolutePath().toString().equals(dst.getAbsolutePath().toString())){

            return true;

        }else{
            InputStream is=new FileInputStream(src);
            OutputStream os=new FileOutputStream(dst);
            byte[] buff=new byte[1024];
            int len;
            while((len=is.read(buff))>0){
                os.write(buff,0,len);
            }
            is.close();
            os.close();
        }
        return true;
    }

    public static List<String> setFileContent(Activity activity, File mFile) {

        List<String> list = new ArrayList<>();
        if(mFile.exists()) {
            File[] files = mFile.listFiles();

            try {
                if (files.length > 0) {

                    for (File file : files) {
                        Log.d("TAG", "File name " + file);
                        if (file.getName().endsWith(".mp4")) {
                            list.add(file.getName());
                        }else if(file.getName().endsWith(".mp3"))
                        {
                            list.add(file.getName());
                        }


                    }
                }


            } catch (Exception e) {
                e.getMessage();
            }
        }else
        {
            list = new ArrayList<>();
            Toast.makeText(activity, "No media files", Toast.LENGTH_SHORT).show();
        }

        return list;


    }

    //**********************************************************************************************

    private Activity mActivity = null;
    private long mUiThreadId = -1;

    public void init(Activity activity) {
        mActivity = activity;
        mUiThreadId = Thread.currentThread().getId();
    }


    public void toast(final String message){
        if (Thread.currentThread().getId() == mUiThreadId) {
            Toast.makeText(mActivity, message, Toast.LENGTH_SHORT).show();
        } else {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(mActivity, message, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public static String getDateTime(Date timestamp){
        DateFormat dateformat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
        dateformat.setTimeZone(TimeZone.getDefault());
        String timeString = timestamp != null ? dateformat.format(timestamp) : "N/A";
        return timeString;
    }

    public boolean isOnline(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    public  boolean validatePhone(String phoneNo) {
        //validate phone numbers of format "1234567890"
        boolean valid ;
        String preg = "((?:\\+27|27)|0)(=99|72|82|73|83|74|84|86|79|81|71|76|60|61|62|63|64|78|)(\\d{7})";

        if (Pattern.matches(preg,phoneNo)) {

            valid = true;
        }else
        {
            valid = false;
        }
        return valid;

    }

    public  String getSex(String idNumber) {
        String M = "Male", F = "Female";

        int d = Integer.parseInt(idNumber.substring(6, 7));
        if (d <= 4 || d == 0) {
            return F;
        } else {
            return M;
        }
    }

    public boolean isIDValid(String idNumber) {
        //TODO: Replace this with your own logic
        //String preg = "([0-9]){2}([0-1][0-9])([0-3][0-9])([0-9]){4}([0-1])([0-9]){2}?";
        String preg = "([0-9]){2}([0-1][0-9])([0-3][0-9])([0-9]){4}([0-1])([8]){1}([0-9]){1}?";
        boolean valid;
        if (Pattern.matches(preg, idNumber))
        {
            if(idNumber.length() == 13)
            {
                valid = true;
            }else
            {
                valid = false;
            }

        }else {
            valid = false;
        }



        return valid;
    }

    public void sendMySMS(String phone,String message) {


        SmsManager sms = SmsManager.getDefault();

        // if message length is too long messages are divided
        List<String> messages = sms.divideMessage(message);
        for (String msg : messages) {

            PendingIntent sentIntent = PendingIntent.getBroadcast(mActivity, 0, new Intent("SMS_SENT"), 0);
            PendingIntent deliveredIntent = PendingIntent.getBroadcast(mActivity, 0, new Intent("SMS_DELIVERED"), 0);
            sms.sendTextMessage(phone, null, msg, sentIntent, deliveredIntent);

        }

    }


    public enum SubcriptionName {
        UNKNOWN        ("UNKNOWN"),
        CELLC        ("Cell C"),
        VODACOM      ("Vodacom"),
        MTN     ("MTN");

        SubcriptionName(String text){
            display = text;
        }
        private String display;

        @Override
        public String toString() {
            return display;
        }


        public static SubcriptionName valueOfName(String name){
            SubcriptionName subName = UNKNOWN;
            for (SubcriptionName scrname : values()){
                if (scrname.toString().equals(name)){
                    subName = scrname;
                    break;
                }
            }
            return subName;
        }

        public static List<SubcriptionName> selectableValues(){
            List<SubcriptionName> selectable = new ArrayList<>();
            for (SubcriptionName level : values()){
                if (level != UNKNOWN){
                    selectable.add(level);
                }
            }
            return selectable;
        }
    }

    public boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        boolean valid;
        String emailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        if (Pattern.matches(emailPattern, email))
        {

            valid = true;


        }else {
            valid = false;
        }

        return valid;
    }

    public boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic

        return password.length() > 4;
    }





}
